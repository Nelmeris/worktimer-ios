//
//  ApiCore.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 09.07.2021.
//

import Foundation
import Moya
import Alamofire

typealias APICoreRequest = Cancellable
typealias APIMethods = TargetType & AccessTokenAuthorizable

struct APICoreError: Codable {
    var code: Int?
    let error: String?
    
    enum ErrorType {
        case decode
        case empty
    }
    
    init(type: ErrorType) {
        switch type {
        case .decode:
            self.error = "Decode error"
        case .empty:
            self.error = "Empty error"
        }
    }
}

extension APICore {
    enum StatusHandler<T: Decodable> {
        case success(_ result: T)
        case failed(_ error: APICoreError)
    }
}

final class APICore<Methods: APIMethods> {
    private let provider: MoyaProvider<Methods>
    private var processingRequests: [String: (() -> Void)] = [:]
    private var refreshCallback: () -> Void
    
    init(availableUrls: [String],
         requestTimeout: TimeInterval,
         authPlugin: AccessTokenPlugin,
         refreshCallback: @escaping () -> Void) {
        
        let serverTrustManager = ServerTrustManager(
            evaluators: availableUrls
                .reduce([String: ServerTrustEvaluating]()) { (dict, url) in
                var dict = dict
                dict[url] = DisabledTrustEvaluator()
                return dict
            }
        )

        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForResource = requestTimeout
        configuration.timeoutIntervalForRequest = requestTimeout
        configuration.httpAdditionalHeaders?["Accept"] = "application/json"
        
        let session = Session(
            configuration: configuration,
            startRequestsImmediately: false,
            serverTrustManager: serverTrustManager
        )

        provider = MoyaProvider<Methods>(session: session, plugins: [authPlugin])

        self.refreshCallback = refreshCallback
    }
    
    @discardableResult
    func request<T: Decodable>(_ endpoint: Methods,
                               of type: T.Type,
                               _ completion: @escaping (_ status: StatusHandler<T>) -> Void) -> APICoreRequest {
        
        let key = stringKey(from: endpoint)
        processingRequests[key] = { [weak self] in
            self?.request(endpoint, of: type, completion)
        }
        let req = provider.request(endpoint) { [weak self] result in
            if let self = self {
                switch result {
                case .success(let response):
                    self.processingRequests[key] = nil
                    do {
                        let decoder = JSONDecoder()
                        decoder.keyDecodingStrategy = .convertFromSnakeCase
                        let decoded = try response.map(T.self, using: decoder)
                        completion(.success(decoded))
                    } catch {
                        completion(.failed(APICoreError(type: .decode)))
                    }
                case .failure(let error):
                    if let response = error.response {
                        if endpoint.authorizationType != .none {
                            self.processingRequests[key] = nil
                        }
                        if error.response?.statusCode == 401 && endpoint.authorizationType != .none {
                            self.refreshCallback()
                        } else {
                            do {
                                self.processingRequests[key] = nil
                                var err = try response.map(APICoreError.self)
                                err.code = response.statusCode
                                completion(.failed(err))
                            } catch {
                                self.processingRequests[key] = nil
                                var err = APICoreError(type: .empty)
                                err.code = response.statusCode
                                completion(.failed(err))
                            }
                        }
                    } else {
                        self.processingRequests[key] = nil
                        completion(.failed(APICoreError(type: .empty)))
                    }
                }
            }
        }
        return req
    }
 
    private func stringKey(from endpoint: Methods) -> String {
        endpoint.method.rawValue + endpoint.baseURL.absoluteString + endpoint.path + String(Date().hashValue)
    }
    
    func reloadWaitTokenRequests() {
        processingRequests.forEach { item in
            processingRequests.removeValue(forKey: item.key)
            item.value()
        }
    }
    
    func clearRequests() {
        processingRequests.removeAll()
    }
    
    static func encodeToParams<T: Encodable>(_ object: T) -> [String: Any] {
        do {
            let encoder = JSONEncoder()
            let data = try encoder.encode(object)
            guard let dict = try JSONSerialization.jsonObject(
                    with: data,
                    options: .allowFragments) as? [String : Any] else {
                fatalError("Смотри модель пес!")
            }
            return dict
        } catch {
            fatalError("Смотри модель пес еще раз!")
        }
    }
}
