//
//  StyleButton.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import UIKit

extension StyleButton {
    enum State: CaseIterable {
        case `default`
        case highlighted
        case disabled
    }
}

class StyleButton: DesignableButton {
    var styleState: State = .default
    
    public override var isHighlighted: Bool {
        didSet { setState(isHighlighted ? .highlighted : .default) }
    }
    
    public override var isEnabled: Bool {
        didSet { setState(isEnabled ? .default : .disabled) }
    }
    
    var titleColorForState: [State: UIColor?] { [:] }
    var backgroundColorForState: [State: UIColor?] { [:] }
    
    var customCornerRadius: CGFloat { 2 }
    var styleFont: UIFont? { StyleLabel().font }
    
    var shadowOpacityForState: [State: Float] { [:] }
    var shadowRadiusForState: [State: CGFloat] { [:] }
    var shadowOffsetForState: [State: CGSize] { [:] }
    var shadowColorForState: [State: UIColor] { [:] }
    
    var customBorderWidth: CGFloat { borderWidth }
    var borderColorForState: [State: UIColor?] { titleColorForState }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        configure()
    }
    
    func configure() {
        titleLabel?.font = styleFont
        cornerRadius = customCornerRadius
        for (state, color) in titleColorForState {
            setTitleColor(color, for: controlState(by: state))
        }
        borderWidth = customBorderWidth
        setState(.default)
    }
    
    func setState(_ state: State) {
        UIView.setAnimationsEnabled(false)
        shadowOffset = shadowOffsetForState[state] ?? shadowOffsetForState[.default] ?? shadowOffset
        shadowColor = shadowColorForState[state] ?? shadowColorForState[.default] ?? shadowColor
        shadowRadius = shadowRadiusForState[state] ?? shadowRadiusForState[.default] ?? shadowRadius
        shadowOpacity = shadowOpacityForState[state] ?? shadowOpacityForState[.default] ?? shadowOpacity
        borderColor = borderColorForState[state] ?? borderColorForState[.default] ?? borderColor
        backgroundColor = backgroundColorForState[state] ?? backgroundColorForState[.default] ?? nil
        layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
    }
    
    func controlState(by state: State) -> UIControl.State {
        switch state {
        case .default: return .normal
        case .disabled: return .disabled
        case .highlighted: return .highlighted
        }
    }
}
