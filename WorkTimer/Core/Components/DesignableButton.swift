//
//  DesignableButton.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import UIKit

open class DesignableButton: UIButton {
    private var defaultBounds: CGRect = .zero
    
    // MARK: - Corner Radius
    
    @IBInspectable public var isRounded: Bool = false {
        didSet { refreshCornerRadius() }
    }
    
    open override var cornerRadius: CGFloat {
        didSet { cachedCornerRadius = cornerRadius }
    }
    
    private var cachedCornerRadius: CGFloat = 0
    
    // MARK: - Shadow
    
    public var isShadowed: Bool {
        shadowView.shadowOpacity > 0
    }
    
    public override var shadowColor: UIColor? {
        get { shadowView.shadowColor }
        set { shadowView.shadowColor = newValue }
    }
    
    public override var shadowOffset: CGSize {
        get { shadowView.shadowOffset }
        set { shadowView.shadowOffset = newValue }
    }
    
    public override var shadowOpacity: Float {
        get { shadowView.shadowOpacity }
        set {
            shadowView.shadowOpacity = newValue
            updateShadowVisibility()
        }
    }
    
    public override var shadowRadius: CGFloat {
        get { shadowView.shadowRadius }
        set { shadowView.shadowRadius = newValue }
    }
    
    // View, а не layer, т.к. у кнопок есть осложнение в виде bgColor/Image для разных state
    // А layer нужно иметь fillColor == backgroundColor, чтобы у кнопки не сбивался цвет
    // Также при работе с layer subviews не обрезаются, т.к. мы не можем установить clipsToBounds = true
    private lazy var shadowView: UIView = {
        let view = UIView(frame: frame)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.accessibilityLabel = "Shadow View"
        view.backgroundColor = .clear
        return view
    }()
    
    open override var isHidden: Bool {
        didSet { shadowView.isHidden = isHidden }
    }
    
    open override var alpha: CGFloat {
        didSet { shadowView.alpha = alpha }
    }
    
    open override var transform: CGAffineTransform {
        didSet { shadowView.transform = transform }
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        updateShadowVisibility()
        guard defaultBounds.height != bounds.height || defaultBounds.width != bounds.width else { return }
        defaultBounds = bounds
        if isRounded {
            refreshCornerRadius()
        }
        refreshShadow()
    }
    
    open override func removeFromSuperview() {
        super.removeFromSuperview()
        shadowView.removeFromSuperview()
    }
    
    private func refreshCornerRadius() {
        super.cornerRadius = !isRounded ? cachedCornerRadius : frame.height / 2
        refreshShadow()
    }
    
    private func refreshShadow() {
        shadowView.layer.shadowPath = UIBezierPath(roundedRect: bounds,
                                                   cornerRadius: cornerRadius).cgPath
    }
    
    private func updateShadowVisibility() {
        guard shadowView.superview != self.superview, isShadowed else { return }
        superview?.insertSubview(shadowView, belowSubview: self)
        NSLayoutConstraint.activate([
            shadowView.topAnchor.constraint(equalTo: topAnchor),
            shadowView.leftAnchor.constraint(equalTo: leftAnchor),
            shadowView.rightAnchor.constraint(equalTo: rightAnchor),
            shadowView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
}
