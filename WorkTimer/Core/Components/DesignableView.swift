//
//  DesignableView.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import UIKit

open class DesignableView: UIView {
    private var defaultBounds: CGRect = .zero
    private lazy var tapViewAnimator = TapViewAnimator()
    private var touchTimer: Timer?
    
    @IBInspectable public var tapAnimationEnabled: Bool = false
    
    // MARK: - Corner Radius
    
    @IBInspectable public var isRounded: Bool = false {
        didSet { refreshCornerRadius() }
    }
    
    open override var cornerRadius: CGFloat {
        didSet { cachedCornerRadius = cornerRadius; refreshCornerRadius() }
    }
    
    @IBInspectable public var cornerTopLeft: Bool = true {
        didSet { refreshCornerRadius() }
    }
    
    @IBInspectable public var cornerTopRight: Bool = true {
        didSet { refreshCornerRadius() }
    }
    
    @IBInspectable public var cornerBottomLeft: Bool = true {
        didSet { refreshCornerRadius() }
    }
    
    @IBInspectable public var cornerBottomRight: Bool = true {
        didSet { refreshCornerRadius() }
    }
    
    private var cachedCornerRadius: CGFloat = 0
    
    // MARK: - Shadow
    
    public var isShadowed: Bool {
        shadowView.shadowOpacity > 0
    }
    
    public override var shadowColor: UIColor? {
        get { shadowView.shadowColor }
        set { shadowView.shadowColor = newValue }
    }
    
    public override var shadowOffset: CGSize {
        get { shadowView.shadowOffset }
        set { shadowView.shadowOffset = newValue }
    }
    
    public override var shadowOpacity: Float {
        get { shadowView.shadowOpacity }
        set {
            shadowView.shadowOpacity = newValue
            updateShadowVisibility()
        }
    }
    
    public override var shadowRadius: CGFloat {
        get { shadowView.shadowRadius }
        set { shadowView.shadowRadius = newValue }
    }
    
    // View, а не layer, т.к. при работе с layer subviews не обрезаются, а мы не можем установить clipsToBounds = true
    private lazy var shadowView: UIView = {
        let view = UIView(frame: frame)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.accessibilityLabel = "Shadow View"
        view.backgroundColor = .clear
        return view
    }()
    
    open override var isHidden: Bool {
        didSet { shadowView.isHidden = isHidden }
    }
    
    open override var alpha: CGFloat {
        didSet { shadowView.alpha = alpha }
    }
    
    open override var transform: CGAffineTransform {
        didSet { shadowView.transform = transform }
    }
    
    // MARK: - Lifecycle
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        updateShadowVisibility()
        guard defaultBounds.height != bounds.height || defaultBounds.width != bounds.width else { return }
        defaultBounds = bounds
        if isRounded {
            refreshCornerRadius()
        }
        refreshShadowPath()
    }
    
    open override func removeFromSuperview() {
        super.removeFromSuperview()
        shadowView.removeFromSuperview()
    }
    
    // MARK: - Tap Animation
    // Работает - не трогай!
    
    override open func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        defer { super.touchesBegan(touches, with: event) }
        guard tapAnimationEnabled else { return }
        touchTimer = Timer.scheduledTimer(withTimeInterval: 0.2, repeats: false) { [weak self] _ in
            guard let self = self else { return }
            self.tapViewAnimator.startAnimation(view: self)
        }
    }

    override open func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        defer { super.touchesEnded(touches, with: event) }
        touchTimer?.invalidate()
        guard tapAnimationEnabled, tapViewAnimator.pressed else { return }
        tapViewAnimator.endAnimation(view: self)
    }

    override open func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        defer { super.touchesCancelled(touches, with: event) }
        touchTimer?.invalidate()
        guard tapAnimationEnabled, tapViewAnimator.pressed else { return }
        tapViewAnimator.endAnimation(view: self)
    }
    
    private func refreshCornerRadius() {
        super.cornerRadius = !isRounded ? cachedCornerRadius : frame.height / 2
        setMaskedCorners(topLeft: cornerTopLeft,
                         topRight: cornerTopRight,
                         bottomLeft: cornerBottomLeft,
                         bottomRight: cornerBottomRight)
        refreshShadowPath()
    }
    
    private func refreshShadowPath() {
        let roundingCorners = makeRectCorners(topLeft: cornerTopLeft,
                                              topRight: cornerTopRight,
                                              bottomLeft: cornerBottomLeft,
                                              bottomRight: cornerBottomRight)
        shadowView.layer.shadowPath = UIBezierPath(roundedRect: bounds,
                                                   byRoundingCorners: roundingCorners,
                                                   cornerRadii: .init(width: cornerRadius,
                                                                      height: cornerRadius)).cgPath
    }
    
    private func updateShadowVisibility() {
        guard shadowView.superview != self.superview, isShadowed else { return }
        superview?.insertSubview(shadowView, belowSubview: self)
        NSLayoutConstraint.activate([
            shadowView.topAnchor.constraint(equalTo: topAnchor),
            shadowView.leftAnchor.constraint(equalTo: leftAnchor),
            shadowView.rightAnchor.constraint(equalTo: rightAnchor),
            shadowView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
}
