//
//  NibView.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import UIKit

open class NibView: UIView {
    private var contentView: UIView!
    private var nibName: String { String(describing: type(of: self)) }
    
    public init() {
        super.init(frame: .zero)
        loadViewFromNib()
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib()
    }
    
    public func loadViewFromNib() {
        let bundle = Bundle(for: type(of: self))
        let nibs = bundle.loadNibNamed(nibName, owner: self)
        contentView = nibs?.first as? UIView
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        contentView.frame = bounds
        backgroundColor = .clear
        addSubview(contentView)
    }
}
