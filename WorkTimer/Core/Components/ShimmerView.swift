//
//  ShimmerView.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import UIKit

protocol ShimmerViewProtocol {
    var position: Int { get set }
    var skeletonView: SkeletonView! { get }
    var rootContainer: UIStackView! { get }
    func setShimmerVisible(_ visible: Bool)
}

extension ShimmerViewProtocol {
    func setShimmerVisible(_ visible: Bool) {
        rootContainer.isHidden = visible
        skeletonView.isHidden = !visible
        if visible {
            skeletonView.startAnimation()
        } else {
            skeletonView.stopAnimation()
        }
    }
}
