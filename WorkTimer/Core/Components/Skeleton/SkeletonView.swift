//
//  SkeletonView.swift
//  Treolan
//
//  Created by Артем Куфаев on 3/7/21.
//  Copyright © 2021 Treolan. All rights reserved.
//

import UIKit

extension SkeletonView {
    public struct ViewModel {
        let type: SkeletonType
        let direction: SkeletonGradientDirection
        
        public init(type: SkeletonType = .gradient,
                    direction: SkeletonGradientDirection = .leftRight) {
            self.type = type
            self.direction = direction
        }
    }
}

public class SkeletonView: UIView {
    
    private var layers: [SkeletonLayer] = []
    private let silverColor = UIColor(red: CGFloat(227) / 255,
                                      green: CGFloat(231) / 255,
                                      blue: CGFloat(231) / 255,
                                      alpha: 1)
    private var viewModel: ViewModel?
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        layers.forEach { $0.contentLayer.frame = self.bounds }
    }
    
    public func prepare(with viewModel: ViewModel) {
        self.viewModel = viewModel
        setSkeletoneLayer(on: self)
    }
    
    public func startAnimation() {
        guard let viewModel = viewModel else { return }
        layers.forEach {
            let animation = $0.type.defaultLayerAnimation(direction: viewModel.direction)
            $0.startAnimation(animation)
        }
    }
    
    public func stopAnimation() {
        layers.forEach { $0.stopAnimation() }
    }
    
    private func setSkeletoneLayer(on view: UIView) {
        guard let model = viewModel else { return }
        view.subviews.forEach { setSkeletoneLayer(on: $0) }
        guard view.subviews.count == 0 else { return }
        let color = silverColor
        view.backgroundColor = nil
        let skeletonLayer = SkeletonLayer(type: model.type, colors: color.makeGradient(), skeletonHolder: view)
        view.layer.insertSublayer(skeletonLayer.contentLayer, at: 0)
        layers.append(skeletonLayer)
    }
    
}

extension UIColor {
    func makeGradient() -> [UIColor] {
        [self, self.complementaryColor, self]
    }
    
    var complementaryColor: UIColor {
        isLight() ? darker : lighter
    }
    
    func isLight() -> Bool {
        guard let components = cgColor.components,
            components.count >= 3 else { return false }
        let brightness = ((components[0] * 299) + (components[1] * 587) + (components[2] * 114)) / 1000
        return !(brightness < 0.5)
    }
    
    public var lighter: UIColor {
        return adjust(by: 1.35)
    }
    
    public var darker: UIColor {
        return adjust(by: 0.94)
    }
    
    func adjust(by percent: CGFloat) -> UIColor {
        var h: CGFloat = 0, s: CGFloat = 0, b: CGFloat = 0, a: CGFloat = 0
        getHue(&h, saturation: &s, brightness: &b, alpha: &a)
        return UIColor(hue: h, saturation: s, brightness: b * percent, alpha: a)
    }
}
