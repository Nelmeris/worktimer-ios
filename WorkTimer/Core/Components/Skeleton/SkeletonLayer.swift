//
//  SkeletonLayer.swift
//  Treolan
//
//  Created by Артем Куфаев on 3/7/21.
//  Copyright © 2021 Treolan. All rights reserved.
//

import UIKit

public enum SkeletonType {
    case solid
    case gradient

    var layer: CALayer {
        switch self {
        case .solid: return CALayer()
        case .gradient: return CAGradientLayer()
        }
    }

    func defaultLayerAnimation(direction: SkeletonGradientDirection? = nil) -> SkeletonLayerAnimation {
        switch self {
        case .solid:
            return { $0.pulse }
        case .gradient:
            let direction = direction ?? .leftRight
            let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: direction)
            return animation
        }
    }
}

struct SkeletonLayer {
    private var maskLayer: CALayer

    var type: SkeletonType { maskLayer is CAGradientLayer ? .gradient : .solid }

    var contentLayer: CALayer { maskLayer }

    init(type: SkeletonType, colors: [UIColor], skeletonHolder holder: UIView) {
        self.maskLayer = type.layer
        self.maskLayer.anchorPoint = .zero
        self.maskLayer.bounds = holder.bounds
        self.maskLayer.cornerRadius = holder.cornerRadius
        self.maskLayer.tint(withColors: colors)
    }
    
    func startAnimation(_ anim: SkeletonLayerAnimation? = nil, completion: (() -> Void)? = nil) {
        let animation = anim ?? type.defaultLayerAnimation()
        contentLayer.playAnimation(animation, key: "skeletonAnimation", completion: completion)
    }

    func stopAnimation() {
        contentLayer.stopAnimation(forKey: "skeletonAnimation")
    }
    
}

fileprivate extension CALayer {
    @objc func tint(withColors colors: [UIColor]) {
        backgroundColor = colors.first?.cgColor
    }
}

fileprivate extension CAGradientLayer {
    override func tint(withColors colors: [UIColor]) {
        self.colors = colors.map { $0.cgColor }
    }
}
