//
//  TableView.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import UIKit

protocol TableViewDataSource: NSObjectProtocol {
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath)
}

protocol PreparebleCell: UITableViewCell {
    associatedtype ViewModel
    func prepare(with model: ViewModel)
}

class TableView<Cell: PreparebleCell>: UITableView, UITableViewDataSource {
    
    var shimmersCount = 10
    private var models: [Cell.ViewModel]?
    private var shimmerVisible = false
    
    weak var dataSourceDelegate: TableViewDataSource?
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        dataSource = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setModels(_ models: [Cell.ViewModel]) {
        self.models = models
        reloadData()
    }
    
    func setShimmerVisible(_ visible: Bool) {
        guard shimmerVisible != visible else { return }
        shimmerVisible = visible
        allowsSelection = !visible
        isScrollEnabled = !visible
        reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if Cell.self is ShimmerViewProtocol.Type, shimmerVisible {
            return shimmersCount
        } else {
            return models?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.getCell(Cell.self)
        if var cell = cell as? ShimmerViewProtocol {
            cell.position = indexPath.row
            cell.setShimmerVisible(shimmerVisible)
        }
        guard let model = models?.element(indexPath.row) else {
            return cell
        }
        cell.prepare(with: model)
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        dataSourceDelegate?.tableView(tableView, canEditRowAt: indexPath) ?? false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        dataSourceDelegate?.tableView(tableView, commit: editingStyle, forRowAt: indexPath)
    }
    
}
