//
//  StyleLabel.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import UIKit

class StyleLabel: UILabel {
    var fontSize: CGFloat { font.pointSize }
    var fontWeight: UIFont.Weight { .regular }
    var lineHeight: CGFloat { fontSize }
    var letterSpacing: CGFloat { 0 }
    var isUppercase: Bool { false }
    var striked: Bool { false }
    
    public init() {
        super.init(frame: .zero)
        commonInit()
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        font = UIFont.systemFont(ofSize: fontSize, weight: fontWeight)
        setLetterSpacing(letterSpacing)
        setLineHeight(lineHeight)
        if striked { setStriked() }
    }
    
    public override var text: String? {
        didSet {
            if isUppercase, text != text?.uppercased() { text = text?.uppercased() }
            setLetterSpacing(letterSpacing)
            setLineHeight(lineHeight)
            if striked { setStriked() }
        }
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        commonInit()
    }
}

extension UIFont.Weight {
    init(number: Int) {
        switch number {
        case 100: self = .ultraLight
        case 200: self = .thin
        case 300: self = .light
        case 400: self = .regular
        case 500: self = .medium
        case 600: self = .semibold
        case 700: self = .bold
        case 800: self = .heavy
        case 900: self = .black
        default: self = .regular
        }
    }
}

extension UILabel {
    public func setLineHeight(_ height: CGFloat) {
        guard let text = self.text, !text.isEmpty, height != font.pointSize else { return }
        
        let attributedString: NSMutableAttributedString
        if let labelAttributedText = attributedText {
            attributedString = NSMutableAttributedString(attributedString: labelAttributedText)
        } else {
            attributedString = NSMutableAttributedString(string: text)
        }
        
        let style: NSMutableParagraphStyle
        
        if let staticStyle = attributedString.attribute(.paragraphStyle,
                                                        at: 0,
                                                        effectiveRange: nil) as? NSParagraphStyle,
           let mutableCopy = staticStyle.mutableCopy() as? NSMutableParagraphStyle {
            style = mutableCopy
        } else {
            style = NSMutableParagraphStyle()
        }
        
        style.minimumLineHeight = height
        
        attributedString.addAttribute(.paragraphStyle,
                                      value: style,
                                      range: NSMakeRange(0, attributedString.length))
        
        // BaselineOffset центрирует текст в строке по вертикали
        // https://stackoverflow.com/questions/56973156/nsattributedstring-text-always-sticks-to-bottom-with-big-lineheight
        let offset = (height - font.lineHeight) / 4
        
        attributedString.addAttribute(.baselineOffset,
                                      value: offset,
                                      range: NSMakeRange(0, attributedString.length))
        
        self.attributedText = attributedString
    }
    
    func setLetterSpacing(_ spacing: CGFloat = 0.0) {
        guard let labelText = text else { return }
        
        let attributedString: NSMutableAttributedString
        if let labelAttributedText = attributedText {
            attributedString = NSMutableAttributedString(attributedString: labelAttributedText)
        } else {
            attributedString = NSMutableAttributedString(string: labelText)
        }
        
        attributedString.addAttribute(NSAttributedString.Key.kern,
                                      value: spacing,
                                      range: NSMakeRange(0, attributedString.length))
        attributedText = attributedString
    }
    
    func setStriked() {
        guard let labelText = text else { return }
        
        let attributedString: NSMutableAttributedString
        if let labelAttributedText = attributedText {
            attributedString = NSMutableAttributedString(attributedString: labelAttributedText)
        } else {
            attributedString = NSMutableAttributedString(string: labelText)
        }
        
        attributedString.addAttribute(.strikethroughStyle, value: 1, range: NSMakeRange(0, attributedString.length))
        attributedText = attributedString
    }
}
