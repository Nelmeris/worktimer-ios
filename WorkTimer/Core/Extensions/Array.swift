//
//  Array.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

extension Array {
    func element(_ index: Int) -> Element? {
        guard 0..<self.count ~= index else { return nil }
        return self[index]
    }
}
