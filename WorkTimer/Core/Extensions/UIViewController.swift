//
//  UIViewController.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import UIKit

private var actionKeyboardHandlerCache: [UIViewController: UIViewController.KeyboardActionHandler] = [:]

extension UIViewController {
    
    public func push(_ controller: UIViewController, animated: Bool = true) {
        navigationController?.pushViewController(controller, animated: animated)
    }
    
    public func push(on controller: UIViewController, animated: Bool = true) {
        controller.push(self, animated: animated)
    }
    
    public func present(on controller: UIViewController, animated: Bool = true) {
        controller.present(self, animated: animated)
    }
    
    public func pushOrPresent(on controller: UIViewController, animated: Bool = true) {
        if controller.navigationController == nil {
            present(on: controller, animated: animated)
        } else {
            push(on: controller, animated: animated)
        }
    }
    
    public func pop() {
        navigationController?.popViewController(animated: true)
    }
    
    public func clearAndPush(_ controller: UIViewController) {
        navigationController?.viewControllers = [controller]
    }

    // MARK: Keyboard
    public func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    public func addHideKeyboardWillAppResignActive() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(dismissKeyboard),
                                               name: UIApplication.willResignActiveNotification,
                                               object: nil)
    }
    
    @objc public func dismissKeyboard() {
        view.endEditing(true)
    }
    
    public enum KeyboardAction {
        case show
        case hide
        case changeFrame
    }
    
    public typealias KeyboardActionHandler = (_ action: KeyboardAction, _ size: CGRect) -> Void
    
    public func subscribeKeyboardEvents(actionHandler: @escaping KeyboardActionHandler) {
        actionKeyboardHandlerCache[self] = actionHandler
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardAction),
                                               name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardAction),
                                               name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardAction),
                                               name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    public func unsubscribeKeyboardEvents() {
        actionKeyboardHandlerCache[self] = nil
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    @objc private func handleKeyboardAction(notification: NSNotification) {
        guard let userInfo = notification.userInfo,
              let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect,
              keyboardSize != .null,
              let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber,
              let actionHandler = actionKeyboardHandlerCache[self]
        else { return }

        UIView.animate(withDuration: TimeInterval(truncating: duration)) {
            switch notification.name {
            case UIResponder.keyboardWillShowNotification:
                actionHandler(.show, keyboardSize)
            case UIResponder.keyboardWillHideNotification:
                actionHandler(.hide, keyboardSize)
            case UIResponder.keyboardWillChangeFrameNotification:
                actionHandler(.changeFrame, keyboardSize)
            default: break
            }
        }
    }

}
