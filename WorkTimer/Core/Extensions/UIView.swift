//
//  UIView+Extensions.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import UIKit

// MARK: - Inspectable Extensions -

public extension UIView {
    
    // MARK: - Border
    
    @IBInspectable var borderWidth: CGFloat {
        get { layer.borderWidth }
        set { layer.borderWidth = newValue }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get { layer.borderColor.map { UIColor(cgColor: $0) } }
        set { layer.borderColor = newValue?.cgColor }
    }
    
    // MARK: - Corner Radius
    
    @IBInspectable var cornerRadius: CGFloat {
        get { layer.cornerRadius }
        set {
            layer.cornerRadius = newValue; checkCorrectUsage()
            // Для активации закругления
            // При закруглении тень отключается
            layer.masksToBounds = cornerRadius > 0
        }
    }
    
    // MARK: - Shadow
    
    @IBInspectable var shadowColor: UIColor? {
        get { layer.shadowColor.map { UIColor(cgColor: $0) } }
        set { layer.shadowColor = newValue?.cgColor }
    }
    
    @IBInspectable var shadowOffset: CGSize {
        get { layer.shadowOffset }
        set { layer.shadowOffset = newValue }
    }
    
    @IBInspectable var shadowOpacity: Float {
        get { layer.shadowOpacity }
        set { layer.shadowOpacity = newValue; checkCorrectUsage() }
    }
    
    @IBInspectable var shadowRadius: CGFloat {
        get { layer.shadowRadius }
        set { layer.shadowRadius = newValue }
    }
    
    private func checkCorrectUsage() {
        let basicTypes = [UIView.self, UIButton.self]
        guard shadowOpacity != 0,
              cornerRadius != 0,
              basicTypes.contains(where: { $0 == type(of: self) }) else { return }
        print("You can't use shadow and corner rounding at the same time. Rounding is a priority. Use custom views")
    }
    
}

// MARK: - Custom Corner radius -

extension UIView {
    public func makeRectCorners(topLeft: Bool, topRight: Bool, bottomLeft: Bool, bottomRight: Bool) -> UIRectCorner {
        switch (topLeft, topRight, bottomLeft, bottomRight) {
        case (true, true, true, true):
            return .allCorners
        case (true, true, true, false):
            return [.topLeft, .topRight, .bottomLeft]
        case (true, true, false, true):
            return [.topLeft, .topRight, .bottomRight]
        case (true, true, false, false):
            return [.topLeft, .topRight]
        case (true, false, true, true):
            return [.topLeft, .bottomLeft, .bottomRight]
        case (true, false, true, false):
            return [.topLeft, .bottomLeft]
        case (true, false, false, true):
            return [.topLeft, .bottomRight]
        case (true, false, false, false):
            return [.topLeft]
        case (false, true, true, true):
            return [.topRight, .bottomLeft, .bottomRight]
        case (false, true, true, false):
            return [.topRight, .bottomLeft]
        case (false, true, false, true):
            return [.topRight, .bottomRight]
        case (false, true, false, false):
            return [.topRight]
        case (false, false, true, true):
            return [.bottomLeft, .bottomRight]
        case (false, false, true, false):
            return [.bottomLeft]
        case (false, false, false, true):
            return [.bottomRight]
        case (false, false, false, false):
            return []
        }
    }
    
    public func setMaskedCorners(topLeft: Bool, topRight: Bool, bottomLeft: Bool, bottomRight: Bool) {
        var maskedCorners: CACornerMask = []
        
        switch (topLeft, topRight, bottomLeft, bottomRight) {
        case (true, true, true, true):
            maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        case (true, true, true, false):
            maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner]
        case (true, true, false, true):
            maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        case (true, true, false, false):
            maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        case (true, false, true, true):
            maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        case (true, false, true, false):
            maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
        case (true, false, false, true):
            maskedCorners = [.layerMinXMinYCorner, .layerMaxXMaxYCorner]
        case (true, false, false, false):
            maskedCorners = [.layerMinXMinYCorner]
        case (false, true, true, true):
            maskedCorners = [.layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        case (false, true, true, false):
            maskedCorners = [.layerMaxXMinYCorner, .layerMinXMaxYCorner]
        case (false, true, false, true):
            maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        case (false, true, false, false):
            maskedCorners = [.layerMaxXMinYCorner]
        case (false, false, true, true):
            maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        case (false, false, true, false):
            maskedCorners = [.layerMinXMaxYCorner]
        case (false, false, false, true):
            maskedCorners = [.layerMaxXMaxYCorner]
        case (false, false, false, false):
            maskedCorners = []
        }
        layer.maskedCorners = maskedCorners
    }
    
    func aspectRation(_ ratio: CGFloat) -> NSLayoutConstraint {
        NSLayoutConstraint(item: self,
                           attribute: .height,
                           relatedBy: .equal,
                           toItem: self,
                           attribute: .width,
                           multiplier: ratio,
                           constant: 0)
    }
    
    func addSubviewFullSize(_ subview: UIView) {
        addSubview(subview)
        subview.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            topAnchor.constraint(equalTo: subview.topAnchor),
            leftAnchor.constraint(equalTo: subview.leftAnchor),
            rightAnchor.constraint(equalTo: subview.rightAnchor),
            bottomAnchor.constraint(equalTo: subview.bottomAnchor),
        ])
    }
}
