//
//  UITableView.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import UIKit

extension UITableView {
    
    private var registeredNibs: [String: UINib] {
        let dict = value(forKey: "_nibMap") as? [String: UINib]
        return dict ?? [:]
    }
    
    // MARK: - Cell
    
    func register<T: UITableViewCell>(cell: T.Type) {
        let name = String(describing: cell.self)
        let bundle = Bundle(for: cell.self)
        register(UINib(nibName: name, bundle: bundle),
                 forCellReuseIdentifier: name)
    }
    
    func getCell<T: UITableViewCell>(_ cellType: T.Type, _ indexPath: IndexPath) -> T {
        let identifier = String(describing: cellType.self)
        if registeredNibs[identifier] == nil {
            register(cell: cellType)
        }
        return dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? T ?? T()
    }
    
    func getCell<T: UITableViewCell>(_ cellType: T.Type) -> T {
        let identifier = String(describing: cellType.self)
        if registeredNibs[identifier] == nil {
            register(cell: cellType)
        }
        return dequeueReusableCell(withIdentifier: identifier) as? T ?? T()
    }
}
