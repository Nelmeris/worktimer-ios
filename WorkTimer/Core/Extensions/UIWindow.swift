//
//  UIWindow.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import UIKit

extension UIWindow {
    func setController(_ controller: UIViewController?, animated: Bool = true) {
        rootViewController = controller
        guard animated else { return }
        // A mask of options indicating how you want to perform the animations.
        let options: UIView.AnimationOptions = .transitionCrossDissolve
        // The duration of the transition animation, measured in seconds.
        let duration: TimeInterval = 0.3
        // Creates a transition animation.
        UIView.transition(with: self, duration: duration, options: options, animations: nil)
    }
    
    static var current: UIWindow { UIApplication.shared.keyWindow! }
}
