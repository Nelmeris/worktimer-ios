//
//  Date.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import Foundation

extension Date {
    static func compareWithoutTime(from: Date?, to: Date?) -> Bool {
        guard let first = from, let second = to else { return to == from }
        let calendar = Calendar.current
        return calendar.compare(first, to: second, toGranularity: .day) == .orderedSame &&
            calendar.compare(first, to: second, toGranularity: .year) == .orderedSame &&
            calendar.compare(first, to: second, toGranularity: .month) == .orderedSame
    }
}
