//
//  Assert.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

func fatalOverride() -> Never {
    fatalError("Необходимо переопределить \(#file) \(#line)")
}
