//
//  TapViewAnimator.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import UIKit

final class TapViewAnimator {
    var pressed = false
    
    func startAnimation(view: UIView, _ completionBlock: @escaping () -> Void = {}) {
        pressed = true
        UIView.animate(withDuration: 0.1,
                       delay: 0,
                       options: .curveLinear,
                       animations: {
                        view.transform = CGAffineTransform(scaleX: 0.95, y: 0.95)
                       }) { _ in
            completionBlock()
        }
    }
    
    func endAnimation(view: UIView, completionBlock: @escaping () -> Void = {}) {
        UIView.animate(withDuration: 0.1,
                       delay: 0,
                       options: .curveLinear,
                       animations: {
                        view.transform = .identity
                       }) { _ in
            self.pressed = false
            completionBlock()
        }
    }
}

public extension UIView {
    func animateTap(startCompletion: @escaping () -> Void = {}) {
        let animator = TapViewAnimator()
        animator.startAnimation(view: self) {
            startCompletion()
            animator.endAnimation(view: self)
        }
    }
}
