//
//  DateFormatter.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import Foundation

public enum DateFormat: String, CaseIterable {
    case type1 = "d MMMM yyyy"                 // 27 февраля 2021
    case type2 = "yyyy-MM-dd"                  // 2021-02-27
    case type3 = "dd.MM.yyyy, HH.mm"           // 27.02.2021, 12:21
    case type4 = "yyyy-MM-dd'T'HH.mm.ss.SSSZ"  // 2021-07-08T12:25:47.390000Z
    
    public func dateFormatter() -> DateFormatter {
        let formatter = DateFormatter()
        formatter.isLenient = true
        formatter.locale = Locale(identifier: "ru_RU")
        formatter.dateFormat = rawValue
        return formatter
    }
    
    public func dateToString(_ date: Date, formatter: DateFormatter) -> String {
        formatter.string(from: date)
    }
        
    private func stringToDate(_ string: String, formatter: DateFormatter) -> Date? {
        formatter.date(from: string)
    }
    
    fileprivate func dateToString(_ date: Date) -> String {
        dateToString(date, formatter: dateFormatter())
    }
    
    fileprivate func stringToDate(_ string: String) -> Date? {
        stringToDate(string, formatter: dateFormatter())
    }
}

public extension String {
    func toDate(format: DateFormat) -> Date? {
        format.stringToDate(self)
    }
    
    func dateString(from: DateFormat, to: DateFormat) -> String? {
        from.stringToDate(self)?.toString(format: to)
    }
}

public extension Date {
    func toString(format: DateFormat) -> String {
        format.dateToString(self)
    }
    
    func stringDate(from: DateFormat, to: DateFormat) -> Date? {
        from.dateToString(self).toDate(format: to)
    }
}
