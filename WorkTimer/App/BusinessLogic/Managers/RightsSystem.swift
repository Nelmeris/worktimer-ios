//
//  RulesSystem.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import Foundation

enum Rights: CaseIterable {
    case addOrganization
    case viewOrganizations
    case updateOrganization
    case deleteOrganization
    
    case addPerson
    case viewPersons
    case updatePerson
    case deletePerson
    
    case addPost
    case viewPosts
    case updatePost
    case deletePost
    
    case addWorkPlace
    case viewWorkPlaces
    case updateWorkPlace
    case deleteWorkPlace
    
    case addWorkTime
    case viewWorkTimes
    case updateWorkTime
    case deleteWorkTime
}

extension Rules {
    var rights: [Rights] {
        switch self {
        case .admin:
            return [
                .addPerson,
                .viewPersons,
                .updatePerson,
                .deletePerson,
                
                .addPost,
                .viewPosts,
                .updatePost,
                .deletePost,
                
                .addWorkTime,
                .viewWorkTimes,
                .updateWorkTime,
                .deleteWorkTime,
                
                .addWorkPlace,
                .viewWorkPlaces,
                .updateWorkPlace,
                .deleteWorkPlace,
            ]
        case .moderator:
            return [
                .viewWorkTimes, .addWorkTime,
                .viewPersons, .addPerson
            ]
        case .supervisor:
            return Rights.allCases
        case .user:
            return [.viewWorkTimes, .addWorkTime]
        }
    }
}

final class RulesSystem {
    private let credentials: AuthCredentials
    
    init(credentials: AuthCredentials) {
        self.credentials = credentials
    }
    
    func available(_ right: Rights) -> Bool {
        credentials.rule.rights.contains(right)
    }
    
    func available(_ rights: [Rights]) -> Bool {
        !rights.map({ available($0) }).contains(false)
    }
    
    func availableLeastOne(_ rights: [Rights]) -> Bool {
        rights.map({ available($0) }).contains(true)
    }
}
