//
//  StorageManager.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import Foundation
import KeychainSwift

extension StorageManager {
    public enum ParameterType {
        case app(StorageType)
        case runtime
    }
    enum StorageType {
        case userDefaults
        case keychain
    }
}

final class StorageManager {
    
    static let shared = StorageManager()
    
    private let userDefaults = UserDefaults.standard
    private let keychain = KeychainSwift()
    private var parametersContainer: [String: Any] = [:]
    
    public func save<T: Codable>(_ value: T?, forKey key: String, type: ParameterType) {
        guard let value = value else {
            remove(forKey: key, type: type)
            return
        }
        switch type {
        case .app(let storageType):
            let data = try? JSONEncoder().encode(value)
            switch storageType {
            case .keychain:
                if let str = value as? String {
                    keychain.set(str, forKey: key)
                } else if let bool = value as? Bool {
                    keychain.set(bool, forKey: key)
                } else if let data = data {
                    keychain.set(data, forKey: key)
                } else {
                    fatalError("Хрень, а не данные")
                }
            case .userDefaults:
                userDefaults.set(data ?? value, forKey: key)
            }
        case .runtime:
            parametersContainer[key] = value
        }
    }
    
    public func load<T: Codable>(forKey key: String, type: ParameterType) -> T? {
        switch type {
        case .app(let storageType):
            let encodableData: Data
            switch storageType {
            case .keychain:
                if let str = keychain.get(key) as? T {
                    return str
                } else if let data = keychain.getData(key) {
                    encodableData = data
                } else {
                    return nil
                }
            case .userDefaults:
                if let data = userDefaults.data(forKey: key) {
                    encodableData = data
                } else if let object = userDefaults.object(forKey: key) as? T {
                    return object
                } else {
                    return nil
                }
            }
            return try? JSONDecoder().decode(T.self, from: encodableData) as T
        case .runtime:
            return parametersContainer[key] as? T
        }
    }
    
    public func remove(forKey key: String, type: ParameterType) {
        switch type {
        case .app(let storageType):
            switch storageType {
            case .keychain:
                keychain.delete(key)
            case .userDefaults:
                userDefaults.removeObject(forKey: key)
            }
        case .runtime:
            parametersContainer[key] = nil
        }
    }
    
}
