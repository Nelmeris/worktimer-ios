//
//  AuthManager.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

final class AuthManager {
    private let loginCredentialsKey = "login_credentials_key"
    private let userCredentialsKey = "user_credentials_key"
    
    private let provider: ServerProvider
    private(set) var rulesSystem: RulesSystem!
    
    var loginCredentials: AuthSystems.LoginCredentials? {
        get { StorageManager.shared.load(forKey: loginCredentialsKey, type: .app(.keychain)) }
        set { StorageManager.shared.save(newValue, forKey: loginCredentialsKey, type: .app(.keychain)) }
    }
    
    var userCredentials: AuthSystems.UserCredentials? {
        get { StorageManager.shared.load(forKey: userCredentialsKey, type: .app(.keychain)) }
        set { StorageManager.shared.save(newValue, forKey: userCredentialsKey, type: .app(.keychain)) }
    }
    
    var authCredentials: AuthCredentials? {
        if let loginCredentials = loginCredentials {
            return AuthCredentials.shared.first(where: {
                switch $0.system {
                case .user: return false
                case .login(let login):
                    return login == loginCredentials
                }
            })
        } else if let userCredentials = userCredentials {
            return .init(system: .user(userCredentials), rule: .user)
        } else {
            return nil
        }
    }
    
    init(provider: ServerProvider) {
        self.provider = provider
        self.rulesSystem = authCredentials.map { .init(credentials: $0) }
    }
    
    func authorize(system: AuthSystems, completion: @escaping (Bool) -> Void) {
        if AuthCredentials.shared.contains(where: {
            $0.system == system
        }) {
            saveAuth(system: system)
            completion(true)
        } else if case .user(let credentials) = system {
            provider.getPersons { [weak self] status in
                switch status {
                case .success(let persons):
                    if persons.contains(where: {
                        $0.lastName == credentials.surname &&
                            $0.birthDate.toString(format: .type2) ==
                            credentials.birthdate.toString(format: .type2)
                    }) {
                        self?.saveAuth(system: .user(credentials))
                        completion(true)
                    } else {
                        completion(false)
                    }
                case .failure(let error):
                    dump(error)
                    completion(false)
                }
            }
        } else {
            completion(false)
        }
    }
    
    func saveAuth(system: AuthSystems) {
        switch system {
        case .login(let credentials):
            self.loginCredentials = credentials
        case .user(let credentials):
            self.userCredentials = credentials
        }
        self.rulesSystem = authCredentials.map { .init(credentials: $0) }
    }
    
    func logout() {
        loginCredentials = nil
        userCredentials = nil
        AppCore.shared.navigation.runFlow(.login)
    }
}
