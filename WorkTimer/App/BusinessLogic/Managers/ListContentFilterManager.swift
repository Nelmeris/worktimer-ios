//
//  ListContentFilterManager.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

extension ListContentFilterManager {
    struct Constants {
        static let organizationTitle = "Организация"
        static let workPlaceTitle = "Рабочее место"
        static let postTitle = "Должность"
        static let personTitle = "Сотрудник"
        static let personStatusTitle = "Статус"
    }
}

final class ListContentFilterManager: BaseFilterManager {
    var rulesSystem: RulesSystem { AppCore.shared.authManager.rulesSystem }
    
    var filter = ListFilter.default
    var organizations: [Organization]?
    var persons: [Person]?
    var workTimes: [WorkTime]?
    var workPlaces: [WorkPlace]?
    var posts: [Post]?
    
    var items: [FilterItemProtocol] {
        var filters = [FilterItemProtocol]()
        
        switch content {
        case .organizations:
            break
        case .workTimes(_, let organizations, let persons):
            filters = addOrganizationsFilter(to: filters, organizations: organizations)
            filters = addPersonsFilter(to: filters, persons: persons)
            
        case .persons(_, let organizations, let workPlaces, let posts):
            filters.append(
                FilterItemView(
                    model: .init(title: Constants.personStatusTitle,
                                 items: Person.Status.allCases.map {
                                    .init(title: $0.rawValue,
                                          checked: filter.statuses?.map({ $0.rawValue }).contains($0.rawValue) ?? false)
                                 },
                                 type: .check)
                )
            )
            filters = addOrganizationsFilter(to: filters, organizations: organizations)
            filters = addWorkPlacesFilter(to: filters, workPlaces: workPlaces)
            filters = addPostsFilter(to: filters, posts: posts)
            
        case .workPlaces(_, let organizations):
            filters = addOrganizationsFilter(to: filters, organizations: organizations)
            
        case .posts(_, let organizations, let workPlaces):
            filters = addWorkPlacesFilter(to: filters, workPlaces: workPlaces)
            filters = addOrganizationsFilter(to: filters, organizations: organizations)
        }
        
        return filters
    }
    
    var dataLoaded: Bool = true
    
    var isDefault: Bool { filter.isDefault }
    let content: ListController.Content
    
    init(content: ListController.Content) {
        self.content = content
        switch content {
        case .organizations(let organizations, let persons):
            self.organizations = organizations
            self.persons = persons
        case .persons(let persons, let organizations, let workPlaces, let posts):
            self.persons = persons
            self.organizations = organizations
            self.workPlaces = workPlaces
            self.posts = posts
        case .workTimes(let workTimes, let organizations, let persons):
            self.workTimes = workTimes
            self.organizations = organizations
            self.persons = persons
        case .workPlaces(let workPlaces, let organizations):
            self.workPlaces = workPlaces
            self.organizations = organizations
        case .posts(let posts, let organizations, let workPlaces):
            self.posts = posts
            self.organizations = organizations
            self.workPlaces = workPlaces
        }
    }
    
    func loadFitlerData(completion: @escaping ([APICoreError]) -> Void) {
        completion([])
    }
    
    func processFilterItems(filters: [FilterItemProtocol]) {
        filters.forEach {
            guard let item = $0 as? FilterItemView else { return }
            switch item.model?.title {
            case Constants.organizationTitle:
                filter.organization = organizations?.first(where: { item.selectedItems.contains($0.name) })
            case Constants.personTitle:
                filter.person = persons?.first(where: { item.selectedItems.contains($0.fio) })
            case Constants.postTitle:
                filter.post = posts?.first(where: { item.selectedItems.contains($0.name) })
            case Constants.personStatusTitle:
                let statuses = Person.Status.allCases.filter({ item.selectedItems.contains($0.rawValue) })
                filter.statuses = statuses.isEmpty ? nil : statuses
            case Constants.workPlaceTitle:
                filter.workPlace = workPlaces?.first(where: { item.selectedItems.contains($0.name) })
            default:
                break
            }
        }
    }
    
    private func addOrganizationsFilter(to filters: [FilterItemProtocol],
                                        organizations: [Organization]) -> [FilterItemProtocol] {
        guard rulesSystem.available(.viewOrganizations) else { return filters }
        var filters = filters
        filters.append(
            FilterItemView(
                model: .init(title: Constants.organizationTitle,
                             items: organizations.map {
                                .init(title: $0.name,
                                      checked: $0.id == filter.organization?.id)
                             },
                             type: .radio)
            )
        )
        return filters
    }
    
    private func addPersonsFilter(to filters: [FilterItemProtocol],
                                  persons: [Person]) -> [FilterItemProtocol] {
        guard rulesSystem.available(.viewPersons) else { return filters }
        var filters = filters
        filters.append(
            FilterItemView(
                model: .init(title: Constants.personTitle,
                             items: persons.map {
                                .init(title: $0.fio,
                                      checked: $0.id == filter.person?.id)
                             },
                             type: .radio)
            )
        )
        return filters
    }
    
    private func addPostsFilter(to filters: [FilterItemProtocol],
                                posts: [Post]) -> [FilterItemProtocol] {
        guard rulesSystem.available(.viewPosts) else { return filters }
        var filters = filters
        filters.append(
            FilterItemView(
                model: .init(title: Constants.postTitle,
                             items: posts.map {
                                .init(title: $0.name,
                                      checked: $0.id == filter.post?.id)
                             },
                             type: .radio)
            )
        )
        return filters
    }
    
    private func addWorkPlacesFilter(to filters: [FilterItemProtocol],
                                     workPlaces: [WorkPlace]) -> [FilterItemProtocol] {
        guard rulesSystem.available(.viewWorkPlaces) else { return filters }
        var filters = filters
        filters.append(
            FilterItemView(
                model: .init(title: Constants.workPlaceTitle,
                             items: workPlaces.map {
                                .init(title: $0.name,
                                      checked: $0.id == filter.workPlace?.id)
                             },
                             type: .radio)
            )
        )
        return filters
    }
    
}

// MARK: - Filter model

struct ListFilter: Equatable {
    var statuses: [Person.Status]?
    var person: Person?
    var organization: Organization?
    var post: Post?
    var workPlace: WorkPlace?
    var keywords: String?
    
    var isDefault: Bool { self == ListFilter.default }
    
    static var `default`: ListFilter {
        return .init()
    }
    
    mutating func reset() {
        statuses = ListFilter.default.statuses
        person = ListFilter.default.person
        organization = ListFilter.default.organization
        post = ListFilter.default.post
        workPlace = ListFilter.default.workPlace
        keywords = ListFilter.default.keywords
    }
    
    static func == (lhs: ListFilter, rhs: ListFilter) -> Bool {
        lhs.statuses == rhs.statuses &&
            lhs.person?.id == rhs.person?.id &&
            lhs.organization?.id == rhs.organization?.id &&
            lhs.post?.id == rhs.post?.id &&
            lhs.workPlace?.id == rhs.workPlace?.id
    }
}
