//
//  BaseFilterManager.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import Foundation

protocol BaseFilterManager {
    var items: [FilterItemProtocol] { get }
    var dataLoaded: Bool { get }
    var isDefault: Bool { get }
    func loadFitlerData(completion: @escaping ([APICoreError]) -> Void)
    func processFilterItems(filters: [FilterItemProtocol])
}
