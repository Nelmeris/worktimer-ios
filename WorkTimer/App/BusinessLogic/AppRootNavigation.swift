//
//  AppRootNavigation.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import UIKit

extension AppRootNavigation {
    enum Flow {
        case login, content
    }
}

class AppRootNavigation {
    private(set) unowned var window: UIWindow!
    private let authManager: AuthManager
    
    init(window: UIWindow, authManager: AuthManager) {
        self.window = window
        self.authManager = authManager
    }

    func start() {
        window.makeKeyAndVisible()
        detectStartFlow()
    }
    
    private func detectStartFlow() {
        if authManager.authCredentials == nil {
            runFlow(.login)
        } else {
            runFlow(.content)
        }
    }
    
    func runFlow(_ flow: Flow) {
        switch flow {
        case .login:
            AuthFlowManager.shared.showLogin()
            
        case .content:
            window.rootViewController = R.storyboard.main.tabBarController()
        }
        
    }
}
