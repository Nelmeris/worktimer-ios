//
//  AppCore.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import UIKit
import Moya

final class AppCore {
    static let shared = AppCore()
    
    lazy var apiCore = APICore<ServerAPI>(availableUrls: ["rouvas.ru"],
                                          requestTimeout: 30,
                                          authPlugin: authPlugin,
                                          refreshCallback: refreshCallback)
    
    lazy var provider = ServerProvider(apiCore: apiCore)
    lazy var authManager = AuthManager(provider: provider)
    private(set) var navigation: AppRootNavigation!
    
    private var authPlugin: AccessTokenPlugin {
        .init { _ in
            Rules.admin.token
        }
    }
    
    private var refreshCallback: () -> Void {
        { fatalError("Бэк потерял токен") }
    }
    
    func prepare(window: UIWindow) {
        navigation = AppRootNavigation(window: window, authManager: authManager)
    }
    
    func run() {
        stylizeTabBar()
        stylizeNavigationBar()
        navigation.start()
    }
    
    private func stylizeNavigationBar() {
        let appearance = UINavigationBar.appearance()
        appearance.titleTextAttributes = [
            .font: TitleLabel().font!,
            .foregroundColor: Color.green
        ]
        appearance.barTintColor = Color.whiteA
        appearance.isTranslucent = false
        appearance.setBackgroundImage(.init(), for: .default)
        appearance.shadowImage = .init()
        appearance.tintColor = Color.green
        appearance.backgroundColor = Color.whiteA
        appearance.backItem?.title = nil
        appearance.shadowRadius = 10
        appearance.shadowOpacity = 0.05
        appearance.backIndicatorImage = R.image.leftArrow()
        appearance.backIndicatorTransitionMaskImage = R.image.leftArrow()
        
        // Скрытие кнопки "Назад"
        UIBarButtonItem.appearance()
            .setBackButtonTitlePositionAdjustment(UIOffset(horizontal: -1000,
                                                           vertical: 0),
                                                  for: .default)
    }
    
    private func stylizeTabBar() {
        let appearance = UITabBar.appearance()
        appearance.tintColor = Color.green
        appearance.unselectedItemTintColor = Color.gray
        appearance.barTintColor = .white
        appearance.shadowImage = UIImage()
        appearance.backgroundImage = UIImage()
        appearance.backgroundColor = Color.whiteA
        appearance.shadowRadius = 10
        appearance.shadowOpacity = 0.05
    }
    
}
