//
//  ServerProvider.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import Foundation

class BaseNetworkProvider<Methods: APIMethods> {
    typealias CompletionHandler<M> = (Status<M>) -> Void
    
    enum Status<M> {
        case success(M)
        case failure(APICoreError)
    }
    
    let apiCore: APICore<Methods>
    
    init(apiCore: APICore<Methods>) {
        self.apiCore = apiCore
    }
    
    func get<BM: DomainModel>(_ endpoint: Methods, completion: @escaping CompletionHandler<[BM]>) {
        apiCore.request(endpoint, of: BaseResponse<[BM.N]>.self) { status in
            switch status {
            case .success(let result):
                let modelsRo = result.results
                completion(.success(modelsRo?.compactMap({ BM(ro: $0) }) ?? []))
            case .failed(let error):
                completion(.failure(error))
            }
        }
    }
}

final class ServerProvider: BaseNetworkProvider<ServerAPI> {
    
    func getOrganizations(completion: @escaping CompletionHandler<[Organization]>) {
        get(.organizations, completion: completion)
    }
    
    func getPersons(completion: @escaping CompletionHandler<[Person]>) {
        get(.persons, completion: completion)
    }
    
    func getPosts(completion: @escaping CompletionHandler<[Post]>) {
        get(.posts, completion: completion)
    }
    
    func getWorkTimes(completion: @escaping CompletionHandler<[WorkTime]>) {
        get(.workTimes, completion: completion)
    }
    
    func getWorkPlaces(completion: @escaping CompletionHandler<[WorkPlace]>) {
        get(.workPlaces, completion: completion)
    }
    
}
