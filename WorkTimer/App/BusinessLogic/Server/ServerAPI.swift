//
//  ServerApi.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 09.07.2021.
//

import Foundation
import Moya

enum ServerAPI {
    case persons
    
    case organizations
    case createOrganization(OrganizationNet)
    
    case posts
    case workPlaces
    case workTimes
}

extension ServerAPI: TargetType {
    public var baseURL: URL {
        URL(string: "http://rouvas.ru/api/v1/")!
    }
    
    public var path: String {
        switch self {
        case .persons: return "persons/"
        case .organizations, .createOrganization: return "organizations/"
        case .posts: return "posts/"
        case .workPlaces: return "work_places/"
        case .workTimes: return "work_times/"
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .organizations, .persons, .workPlaces, .workTimes, .posts:
            return .get
        case .createOrganization:
            return .post
        }
    }
    
    public var task: Task {
        switch self {
        case .createOrganization(let model):
            return .requestJSONEncodable(model)
        default:
            return .requestPlain
        }
    }
    
    public var headers: [String: String]? {
        ["Accept-Encoding": "gzip, deflate, br"]
    }
    
    public var validationType: ValidationType {
        .successCodes
    }
    
    public var sampleData: Data {
        Data()
    }
}

extension ServerAPI: AccessTokenAuthorizable {
    var authorizationType: AuthorizationType? {
        .custom("Token")
    }
}
