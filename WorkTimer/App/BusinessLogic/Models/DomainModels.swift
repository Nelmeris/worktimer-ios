//
//  DomainModels.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import Foundation

protocol DomainModel {
    associatedtype N: Decodable
    
    init?(ro: N)
}

struct Organization: DomainModel {
    let id: Int
    let name: String
    let phone: String
    let ownerId: Int
    
    init?(ro: OrganizationNet) {
        guard let id = ro.id,
              let name = ro.name,
              let phone = ro.phone,
              let ownerId = ro.owner
        else { return nil }
        self.id = id
        self.name = name
        self.phone = phone
        self.ownerId = ownerId
    }
}

struct Person: DomainModel {
    let id: Int
    let firstName: String
    let lastName: String
    let middleName: String
    let phone: String
    let email: String
    let birthDate: Date
    let status: Status
    let comment: String?
    let organizationId: Int
    let workPlaceId: Int
    let postId: Int
    
    var fio: String {
        "\(lastName) \(firstName) \(middleName)"
    }
    
    init?(ro: PersonNet) {
        guard let id = ro.id,
              let firstName = ro.firstName,
              let lastName = ro.lastName,
              let middleName = ro.middleName,
              let phone = ro.phone,
              let email = ro.email,
              let birthDate = ro.birthDate?.toDate(format: .type2),
              let status = ro.status.map({ Status(rawValue: $0) }) ?? nil,
              let organizationId = ro.organization,
              let workPlaceId = ro.workPlace,
              let postId = ro.post
        else { return nil }
        self.id = id
        self.firstName = firstName
        self.lastName = lastName
        self.middleName = middleName
        self.phone = phone
        self.email = email
        self.birthDate = birthDate
        self.status = status
        self.comment = ro.comment
        self.organizationId = organizationId
        self.workPlaceId = workPlaceId
        self.postId = postId
    }
    
    enum Status: String, CaseIterable {
        case work = "Работает"
        case dontWork = "Не работает"
        case onVacation = "В отпуске"
    }
}

struct Post: DomainModel {
    let id: Int
    let name: String
    let organizationId: Int
    let workPlaceId: Int?
    
    init?(ro: PostNet) {
        guard let id = ro.id,
              let name = ro.name,
              let organizationId = ro.organization
        else { return nil }
        self.id = id
        self.name = name
        self.organizationId = organizationId
        self.workPlaceId = ro.workPlace
    }
}

struct WorkPlace: DomainModel {
    let id: Int
    let name: String
    let organizationId: Int
    
    init?(ro: WorkPlaceNet) {
        guard let id = ro.id,
              let name = ro.name,
              let organizationId = ro.organization
        else { return nil }
        self.id = id
        self.name = name
        self.organizationId = organizationId
    }
}

struct WorkTime: DomainModel {
    let id: Int
    let startFrom: Date
    let endAt: Date
    let organizationId: Int
    let personId: Int
    
    init?(ro: WorkTimeNet) {
        guard let id = ro.id,
              let startFrom = ro.startFrom.map({ $0.toDate(format: .type4) }) ?? nil,
              let endAt = ro.endedAt.map({ $0.toDate(format: .type4) }) ?? nil,
              let organizationId = ro.organization,
              let personId = ro.person
        else { return nil }
        self.id = id
        self.startFrom = startFrom
        self.endAt = endAt
        self.organizationId = organizationId
        self.personId = personId
    }
}
