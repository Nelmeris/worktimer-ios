//
//  RequestModels.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import Foundation

struct BaseResponse<Model: Decodable>: Decodable {
    let count: Int?
    let results: Model?
}

struct OrganizationNet: Codable {
    let id: Int?
    let name: String?
    let phone: String?
    let owner: Int?
}

struct PersonNet: Codable {
    let id: Int?
    let firstName: String?
    let lastName: String?
    let middleName: String?
    let phone: String?
    let email: String?
    let birthDate: String?
    let status: String?
    let comment: String?
    let organization: Int?
    let workPlace: Int?
    let post: Int?
}

struct PostNet: Codable {
    let id: Int?
    let name: String?
    let organization: Int?
    let workPlace: Int?
}

struct WorkPlaceNet: Codable {
    let id: Int?
    let name: String?
    let organization: Int?
}

struct WorkTimeNet: Codable {
    let id: Int?
    let startFrom: String?
    let endedAt: String?
    let organization: Int?
    let person: Int?
}
