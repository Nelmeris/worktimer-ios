//
//  AuthCredentials.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import Foundation

struct AuthCredentials {
    let system: AuthSystems
    let rule: Rules
    
    static let shared: [AuthCredentials] = [
        .init(system: .login(.init(login: "supervisor@admin.com", password: "admin")), rule: .supervisor),
        .init(system: .login(.init(login: "admin@admin.com", password: "admin")), rule: .admin),
        .init(system: .login(.init(login: "moder@admin.com", password: "admin")), rule: .moderator),
    ]
}

enum Rules: Int, CaseIterable {
    case supervisor = 2
    case admin = 1
    case moderator = 0
    case user = -1
    
    var token: String {
        switch self {
        case .supervisor:
            return "dab14415f8e82578dd614a4d449447681a6cfe82"
        case .admin:
            return "2cd1dc478c84a6abc1885804ba827d5ea9880aab"
        case .moderator:
            return "2ab028174ba542c38f3f579f57ae56fcd165aba0"
        case .user:
            return "a9358a9f170bac0194ee90692e12816e1e8a6bc8"
        }
    }
}

enum AuthSystems: Equatable {
    case login(LoginCredentials)
    case user(UserCredentials)
    
    struct LoginCredentials: Codable, Equatable {
        let login: String
        let password: String
    }
    
    struct UserCredentials: Codable, Equatable {
        let surname: String
        let birthdate: Date
    }
    
    static func == (lhs: AuthSystems, rhs: AuthSystems) -> Bool {
        switch (lhs, rhs) {
        case (.login(let lhs), .login(let rhs)):
            return lhs == rhs
        case (.user(let lhs), .user(let rhs)):
            return lhs == rhs
        default:
            return false
        }
    }
}
