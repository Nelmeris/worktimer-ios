//
//  Color.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import UIKit

public struct Color {
    public static let black = R.color.black()!
    public static let blackA = R.color.blackA()!
    public static let green = R.color.green()!
    public static let greenPressed = R.color.greenPressed()!
    public static let greenDisabled = R.color.greenDisabled()!
    public static let red = R.color.red()!
    public static let gray = R.color.gray()!
    public static let graySeparator = R.color.graySeparator()!
    public static let white = R.color.white()!
    public static let whiteA = R.color.whiteA()!
}
