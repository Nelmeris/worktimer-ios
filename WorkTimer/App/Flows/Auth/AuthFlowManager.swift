//
//  LoginFlowHelper.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import UIKit

final class AuthFlowManager {
    
    static let shared = AuthFlowManager()
    
    unowned var window: UIWindow!
    
    // MARK: - Show
    
    func showLogin() {
        let vc = LoginController.new { [weak self] in
            self?.handleLoginAction(authSystem: $0, vc: $1)
        }
        AppCore.shared.navigation.window.setController(vc)
    }
    
    // MARK: - B-Logic
    
    private func handleLoginAction(authSystem: AuthSystems, vc: LoginController) {
        AppCore.shared.authManager.authorize(system: authSystem) { isAuthorize in
            if isAuthorize {
                AppCore.shared.navigation.runFlow(.content)
            } else {
                vc.setupFieldsError()
            }
        }
    }
    
}
