//
//  LoginController.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import UIKit

extension LoginController {
    typealias ActionHandler = (AuthSystems, LoginController) -> Void
    
    static func new(actionHandler: @escaping ActionHandler) -> LoginController {
        guard let vc = R.storyboard.auth.loginController() else { fatalError() }
        vc.prepare(actionHandler: actionHandler)
        return vc
    }
    
    enum State {
        case user
        case login
    }
    
    enum VisibleState {
        case short, full
    }
}

final class LoginController: UIViewController {
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var rootContainer: UIStackView!
    @IBOutlet private weak var loginField: TextField!
    @IBOutlet private weak var surnameField: TextField!
    @IBOutlet private weak var passwordField: PasswordField!
    @IBOutlet private weak var dateField: DateField!
    @IBOutlet private weak var changeStateContainer: UIStackView!
    @IBOutlet private weak var changeStateButton: UIButton!
    
    private var state: State = .login
    private var visibleState: VisibleState = .full
    private var bottomMargin: CGFloat!
    
    private var actionHandler: ActionHandler!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bottomMargin = rootContainer.layoutMargins.bottom
        hideKeyboardWhenTappedAround()
        subscribeKeyboardEvents { [weak self] in
            self?.handleKeyboardAction(state: $0, keyboardSize: $1)
        }
        dateField.datePicker.maximumDate = Date()
        refreshUI()
    }
    
    deinit {
        unsubscribeKeyboardEvents()
    }
    
    func prepare(actionHandler: @escaping ActionHandler) {
        self.actionHandler = actionHandler
    }
    
    func setupFieldsError() {
        loginField.borderColor = Color.red
        passwordField.borderColor = Color.red
        dateField.borderColor = Color.red
        surnameField.borderColor = Color.red
    }
    
    private func refreshUI() {
        surnameField.isHidden = state != .user
        dateField.isHidden = state != .user
        passwordField.isHidden = state != .login
        loginField.isHidden = state != .login
        switch state {
        case .login:
            titleLabel.text = "Авторизация за\nадминистратора"
            changeStateButton.setTitle("Войти как сотрудник", for: .normal)
        case .user:
            titleLabel.text = "Авторизация за\nсотрудника"
            changeStateButton.setTitle("Войти как администратор", for: .normal)
        }
    }
    
    private func resetFields() {
        loginField.editingChanged()
        passwordField.editingChanged()
        dateField.editingChanged()
        surnameField.editingChanged()
    }
    
    private func startLogin() {
        switch state {
        case .login:
            guard let login = loginField.text?.trimmingCharacters(in: .whitespacesAndNewlines),
                  let password = passwordField.text
            else {
                setupFieldsError()
                return
            }
            actionHandler(.login(.init(login: login, password: password)), self)
            
        case .user:
            guard let surname = surnameField.text?.trimmingCharacters(in: .whitespacesAndNewlines),
                  let birthDate = dateField.date
            else {
                setupFieldsError()
                return
            }
            actionHandler(.user(.init(surname: surname, birthdate: birthDate)), self)
        }
    }
    
    private func handleKeyboardAction(state: KeyboardAction, keyboardSize: CGRect) {
        self.visibleState = self.visibleState == .short ? .full : .short
        
        let newMargin = self.bottomMargin + (state == .hide ? .zero :
            keyboardSize.height - self.view.safeAreaInsets.bottom)
        guard self.rootContainer.layoutMargins.bottom != newMargin else { return }
        defer { self.view.layoutIfNeeded() }
        self.rootContainer.layoutMargins.bottom = newMargin
        
        let isHidden = state != .hide
        guard self.changeStateContainer.isHidden != isHidden else { return }
        self.changeStateContainer.isHidden = isHidden
    }
    
    @IBAction private func touchUpLogin(_ sender: UIButton) {
        resetFields()
        view.endEditing(true)
        startLogin()
    }
    
    @IBAction private func touchUpChangeState(_ sender: UIButton) {
        state = state == .login ? .user : .login
        resetFields()
        refreshUI()
    }
}
