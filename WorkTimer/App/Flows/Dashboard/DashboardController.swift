//
//  DashboardController.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import UIKit

final class DashboardController: UIViewController {
    @IBOutlet private weak var container: UIStackView!
    
    private weak var organizationsCard: DashboardCard?
    private weak var postsCard: DashboardCard?
    private weak var personsCard: DashboardCard?
    private weak var workTimesCard: DashboardCard?
    private weak var workPlacesCard: DashboardCard?
    
    private var organizations: [Organization]?
    private var posts: [Post]?
    private var persons: [Person]?
    private var workTimes: [WorkTime]?
    private var workPlaces: [WorkPlace]?
    
    var provider: ServerProvider { AppCore.shared.provider }
    var rulesSystem: RulesSystem { AppCore.shared.authManager.rulesSystem }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if rulesSystem.available(.viewOrganizations) {
            provider.getOrganizations { status in
                switch status {
                case .success(let items):
                    self.organizations = items
                    self.organizationsCard = self.addCard(title: "Организации", count: items.count)
                case .failure(let error):
                    dump(error)
                }
            }
        }
        if rulesSystem.available(.viewPosts) {
            provider.getPosts { status in
                switch status {
                case .success(let items):
                    self.posts = items
                    self.postsCard = self.addCard(title: "Должности", count: items.count)
                case .failure(let error):
                    dump(error)
                }
            }
        }
        if rulesSystem.available(.viewPersons) {
            provider.getPersons { status in
                switch status {
                case .success(let items):
                    self.persons = items
                    self.personsCard = self.addCard(title: "Сотрудники", count: items.count)
                case .failure(let error):
                    dump(error)
                }
            }
        }
        if rulesSystem.available(.viewWorkTimes) {
            provider.getWorkTimes { status in
                switch status {
                case .success(let items):
                    self.workTimes = items
                    self.workTimesCard = self.addCard(title: "Рабочее время", count: items.count)
                case .failure(let error):
                    dump(error)
                }
            }
        }
        if rulesSystem.available(.viewWorkPlaces) {
            provider.getWorkPlaces { status in
                switch status {
                case .success(let items):
                    self.workPlaces = items
                    self.workPlacesCard = self.addCard(title: "Рабочие места", count: items.count)
                case .failure(let error):
                    dump(error)
                }
            }
        }
    }
    
    private func addCard(title: String, count: Int) -> DashboardCard {
        let view = DashboardCard()
        view.prepare(with: .init(title: title, count: count), delegate: self)
        container.addArrangedSubview(view)
        return view
    }
    
    private func showList(content: ListController.Content) {
        let vc = ListController.new(content: content,
                                    provider: provider,
                                    filterManager: .init(content: content)) { content in
            
        }
        self.push(vc)
    }
    
}

extension DashboardController: DashboardCardDelegate {
    func dashboardCardTap(card: DashboardCard) {
        switch card {
        case let card where card === self.organizationsCard:
            guard let organizations = self.organizations else { return }
            showList(content: .organizations(organizations, persons ?? []))

        case let card where card === self.postsCard:
            guard let posts = self.posts else { return }
            showList(content: .posts(posts, organizations ?? [], workPlaces ?? []))

        case let card where card === self.personsCard:
            guard let persons = self.persons else { return }
            showList(content: .persons(persons, organizations ?? [], workPlaces ?? [], posts ?? []))

        case let card where card === self.workTimesCard:
            guard let workTimes = self.workTimes else { return }
            showList(content: .workTimes(workTimes, organizations ?? [], persons ?? []))

        case let card where card === self.workPlacesCard:
            guard let workPlaces = self.workPlaces else { return }
            showList(content: .workPlaces(workPlaces, organizations ?? []))

        default:
            break
        }
    }
}
