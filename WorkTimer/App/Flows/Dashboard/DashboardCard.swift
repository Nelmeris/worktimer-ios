//
//  DashboardCard.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import UIKit

protocol DashboardCardDelegate: AnyObject {
    func dashboardCardTap(card: DashboardCard)
}

extension DashboardCard {
    struct ViewModel {
        let title: String
        let count: Int
    }
}

final class DashboardCard: NibView {
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var countLabel: UILabel!
    
    private weak var delegate: DashboardCardDelegate?
    
    func prepare(with model: ViewModel, delegate: DashboardCardDelegate) {
        self.delegate = delegate
        titleLabel.text = model.title
        countLabel.text = String(model.count)
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(touchUp)))
    }
    
    @objc private func touchUp() {
        animateTap {
            self.delegate?.dashboardCardTap(card: self)
        }
    }
}
