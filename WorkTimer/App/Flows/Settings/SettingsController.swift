//
//  SettingsController.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import UIKit

final class SettingsController: UIViewController {
    
    @IBOutlet private weak var fioLabel: UILabel!
    @IBOutlet private weak var phoneLabel: UILabel!
    @IBOutlet private weak var birthdateLabel: UILabel!
    @IBOutlet private weak var statusLabel: UILabel!
    @IBOutlet private weak var organizationLabel: UILabel!
    @IBOutlet private weak var workPlaceLabel: UILabel!
    @IBOutlet private weak var postLabel: UILabel!
    
    private let dispatchGroup = DispatchGroup()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let provider = AppCore.shared.provider
        
        var person: Person?
        var organizations: [Organization]?
        var workPlaces: [WorkPlace]?
        var posts: [Post]?
        
        dispatchGroup.enter()
        provider.getPersons { status in
            switch status {
            case .success(let persons):
                switch AppCore.shared.authManager.authCredentials?.system {
                case .user(let userCredentials):
                    if let pers = persons.first(where: {
                        userCredentials.surname == $0.lastName &&
                            userCredentials.birthdate.toString(format: .type1) == $0.birthDate.toString(format: .type1)
                    }) {
                        person = pers
                    }
                case .login(let loginCredentials):
                    if let pers = persons.first {
                        person = pers
                    }
                case .none:
                    break
                }
            case .failure(let error):
                dump(error)
            }
            self.dispatchGroup.leave()
        }
        
        dispatchGroup.enter()
        provider.getOrganizations { status in
            switch status {
            case .success(let result):
                organizations = result
            case .failure(let error):
                dump(error)
            }
            self.dispatchGroup.leave()
        }
        
        dispatchGroup.enter()
        provider.getWorkPlaces { status in
            switch status {
            case .success(let result):
                workPlaces = result
            case .failure(let error):
                dump(error)
            }
            self.dispatchGroup.leave()
        }
        
        dispatchGroup.enter()
        provider.getPosts { status in
            switch status {
            case .success(let result):
                posts = result
            case .failure(let error):
                dump(error)
            }
            self.dispatchGroup.leave()
        }
        
        dispatchGroup.notify(queue: .main) {
            guard let person = person else { return }
            self.setData(person: person, organizations: organizations ?? [], posts: posts ?? [], workPlaces: workPlaces ?? [])
        }
    }
    
    private func setData(person: Person, organizations: [Organization], posts: [Post], workPlaces: [WorkPlace]) {
        fioLabel.text = person.fio
        phoneLabel.text = person.phone
        birthdateLabel.text = person.birthDate.toString(format: .type1)
        statusLabel.text = person.status.rawValue
        organizationLabel.text = organizations.first { person.organizationId == $0.id }?.name
        postLabel.text = posts.first { person.postId == $0.id }?.name
        workPlaceLabel.text = workPlaces.first { person.workPlaceId == $0.id }?.name
    }
    
    @IBAction private func touchUpLogout(_ sender: Any) {
        AppCore.shared.authManager.logout()
    }
    
}
