//
//  ListController.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import UIKit
extension ListController {
    typealias ActionHandler = (Content) -> Void
    
    enum Content {
        case organizations([Organization], [Person])
        case persons([Person], [Organization], [WorkPlace], [Post])
        case posts([Post], [Organization], [WorkPlace])
        case workPlaces([WorkPlace], [Organization])
        case workTimes([WorkTime], [Organization], [Person])
        
        var title: String {
            switch self {
            case .organizations:
                return "Организации"
            case .persons:
                return "Сотрудники"
            case .posts:
                return "Должности"
            case .workPlaces:
                return "Рабочие места"
            case .workTimes:
                return "Рабочее время"
            }
        }
    }
    
    static func new(content: Content,
                    provider: ServerProvider,
                    filterManager: ListContentFilterManager,
                    actionHandler: @escaping ActionHandler) -> ListController {
        
        guard let vc = R.storyboard.main.listController() else { fatalError() }
        vc.prepare(content: content,
                   provider: provider,
                   filterManager: filterManager,
                   actionHandler: actionHandler)
        return vc
    }
}

final class ListController: BaseContentController<ListItemCell> {
    private var provider: ServerProvider!
    private var rulesSystem: RulesSystem { AppCore.shared.authManager.rulesSystem }
    private var _filterManager: ListContentFilterManager!
    override var filterManager: BaseFilterManager { _filterManager }
    
    override var filterKeywords: String? {
        get { _filterManager.filter.keywords }
        set { _filterManager.filter.keywords = newValue }
    }
    override var searchPlaceholder: String {
        switch content {
        case .organizations, .workPlaces, .workTimes, .posts:
            return "название"
        case .persons:
            return "имя, фамилия, отчество"
        case .none:
            return ""
        }
    }
    
    override var canFilter: Bool {
        switch content {
        case .posts:
            return rulesSystem.availableLeastOne([.viewWorkPlaces, .viewOrganizations])
        case .workPlaces:
            return rulesSystem.available(.viewOrganizations)
        case .workTimes:
            return rulesSystem.availableLeastOne([.viewOrganizations, .viewPersons])
        case .persons:
            return true
        case .none, .organizations:
            return false
        }
    }
    
    override var canSearch: Bool {
        switch content {
        case .organizations, .workPlaces, .persons, .posts:
            return true
        case .workTimes, .none:
            return false
        }
    }
    
    private var actionHandler: ActionHandler!
    private var keyboardState = KeyboardAction.hide
    
    private var content: Content!
    
    func prepare(content: Content,
                 provider: ServerProvider,
                 filterManager: ListContentFilterManager,
                 actionHandler: @escaping ActionHandler) {
        
        self.content = content
        self._filterManager = filterManager
        self.provider = provider
        self.actionHandler = actionHandler
        loadViewIfNeeded()
        title = content.title
        tableView.dataSourceDelegate = self
    }
    
    override func loadData() {
        setModels(content: content)
        setState(.default)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.setSelected(false, animated: true)
//        guard let product = products.element(indexPath.row) else { return }
//        actionHandler(.tapProduct(product), self)
    }
    
    private func setModels(content: Content) {
        switch content {
        case .organizations(let organizations, let persons):
            let filtered = organizations.filter({
                guard let keywords = _filterManager.filter.keywords, !keywords.isEmpty else { return true }
                return $0.name.lowercased().contains(keywords.lowercased())
            })
            tableView.setModels(filtered.map({ model in
                var items: [LineView.ViewModel] = [
                    .init(title: "Название", value: model.name),
                    .init(title: "Телефон", value: model.phone),
                ]
                if let person = persons.first(where: { $0.id == model.ownerId }),
                   rulesSystem.available(.viewPersons)  {
                    items.append(.init(title: "Владелец", value: person.fio))
                }
                return .init(items: items)
            }))
            
        case .persons(let persons, let organizations, let workPlaces, let posts):
            let filtered = persons.filter({
                guard let keywords = _filterManager.filter.keywords, !keywords.isEmpty else { return true }
                return $0.fio.lowercased().contains(keywords.lowercased())
            }).filter({
                return $0.organizationId == _filterManager.filter.organization?.id ?? $0.organizationId &&
                    $0.workPlaceId == _filterManager.filter.workPlace?.id ?? $0.workPlaceId &&
                    $0.postId == _filterManager.filter.post?.id ?? $0.postId &&
                    _filterManager.filter.statuses?.map({ $0.rawValue }).contains($0.status.rawValue) ?? true
            })
            tableView.setModels(filtered.map({ model in
                var items: [LineView.ViewModel] = [
                    .init(title: "ФИО", value: model.fio),
                    .init(title: "Телефон", value: model.phone),
                    .init(title: "Статус", value: model.status.rawValue),
                ]
                if let organization = organizations.first(where: { $0.id == model.organizationId }),
                   rulesSystem.available(.viewOrganizations)  {
                    items.append(.init(title: "Организация", value: organization.name))
                }
                if let workPlace = workPlaces.first(where: { $0.id == model.workPlaceId }),
                   rulesSystem.available(.viewWorkPlaces)  {
                    items.append(.init(title: "Рабочее место", value: workPlace.name))
                }
                if let post = posts.first(where: { $0.id == model.postId }),
                   rulesSystem.available(.viewPosts) {
                    items.append(.init(title: "Должность", value: post.name))
                }
                return .init(items: items)
            }))
            
        case .posts(let posts, let organizations, let workPlaces):
            let filtered = posts.filter({
                guard let keywords = _filterManager.filter.keywords, !keywords.isEmpty else { return true }
                return $0.name.lowercased().contains(keywords.lowercased())
            }).filter({
                return $0.organizationId == _filterManager.filter.organization?.id ?? $0.organizationId &&
                    $0.workPlaceId == _filterManager.filter.workPlace?.id ?? $0.workPlaceId
            })
            tableView.setModels(filtered.map({ model in
                var items: [LineView.ViewModel] = [
                    .init(title: "Название", value: model.name),
                ]
                if let organization = organizations.first(where: { $0.id == model.organizationId }),
                   rulesSystem.available(.viewOrganizations)  {
                    items.append(.init(title: "Организация", value: organization.name))
                }
                if let workPlace = workPlaces.first(where: { $0.id == model.workPlaceId }),
                   rulesSystem.available(.viewWorkPlaces)  {
                    items.append(.init(title: "Рабочее место", value: workPlace.name))
                }
                return .init(items: items)
            }))
            
        case .workPlaces(let workPlaces, let organizations):
            let filtered = workPlaces.filter({
                guard let keywords = _filterManager.filter.keywords, !keywords.isEmpty else { return true }
                return $0.name.lowercased().contains(keywords.lowercased())
            })
            tableView.setModels(filtered.map({ model in
                var items: [LineView.ViewModel] = [
                    .init(title: "Название", value: model.name),
                ]
                if let organization = organizations.first(where: { $0.id == model.organizationId }),
                   rulesSystem.available(.viewOrganizations)  {
                    items.append(.init(title: "Организация", value: organization.name))
                }
                return .init(items: items)
            }))
            
        case .workTimes(let workTimes, let organizations, let persons):
            let filtered = workTimes.filter({
                return $0.personId == _filterManager.filter.person?.id ?? $0.personId &&
                    $0.organizationId == _filterManager.filter.organization?.id ?? $0.organizationId
            })
            tableView.setModels(filtered.map({ model in
                var items: [LineView.ViewModel] = [
                    .init(title: "Время начала", value: model.startFrom.toString(format: .type3)),
                    .init(title: "Время окончания", value: model.endAt.toString(format: .type3)),
                ]
                if let organization = organizations.first(where: { $0.id == model.organizationId }),
                   rulesSystem.available(.viewOrganizations) {
                    items.append(.init(title: "Организация", value: organization.name))
                }
                if let person = persons.first(where: { $0.id == model.personId }),
                   rulesSystem.available(.viewPersons) {
                    items.append(.init(title: "Сотрудник", value: person.fio))
                }
                return .init(items: items)
            }))
        }
    }
    
}

extension ListController: TableViewDataSource {
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        switch editingStyle {
        case .delete:
            switch content {
            case .organizations(var organizations, let persons):
                organizations.remove(at: indexPath.row)
                self.content = .organizations(organizations, persons)
            case .persons(var persons, let organizations, let workPlaces, let posts):
                persons.remove(at: indexPath.row)
                self.content = .persons(persons, organizations, workPlaces, posts)
            case .posts(var posts, let organizations, let workPlaces):
                posts.remove(at: indexPath.row)
                self.content = .posts(posts, organizations, workPlaces)
            case .workPlaces(var workPlaces, let organizations):
                workPlaces.remove(at: indexPath.row)
                self.content = .workPlaces(workPlaces, organizations)
            case .workTimes(var workTimes, let organizations, let persons):
                workTimes.remove(at: indexPath.row)
                self.content = .workTimes(workTimes, organizations, persons)
            case .none:
                return
            }
            setModels(content: content)
        default:
            break
        }
    }
    
    
}
