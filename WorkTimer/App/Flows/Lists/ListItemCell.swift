//
//  ListItemView.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import UIKit

extension ListItemCell {
    struct ViewModel {
        let items: [LineView.ViewModel]
    }
}

final class ListItemCell: UITableViewCell, PreparebleCell {
    @IBOutlet private weak var container: UIStackView!
    
    func prepare(with model: ViewModel) {
        container.removeAllArrangedSubviews()
        model.items.forEach { model in
            let view = LineView()
            view.prepare(with: model)
            container.addArrangedSubview(view)
        }
    }
    
}
