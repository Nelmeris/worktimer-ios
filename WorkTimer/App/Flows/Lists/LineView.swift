//
//  LineView.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import UIKit

extension LineView {
    struct ViewModel {
        let title: String
        let value: String
    }
}

final class LineView: NibView {
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var valueLabel: UILabel!
    
    func prepare(with model: ViewModel) {
        nameLabel.text = model.title
        valueLabel.text = model.value
    }
}
