//
//  DateField.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import UIKit

extension DateField {
    typealias ActionHandler = (DateField, Date) -> Void
}

final class DateField: TextField {
    
    let locale = Locale(identifier: "ru")
    let dateFormat = "dd.MM.yyyy"
    let doneButtonTitle = "Готово"
    let datePickerMode: UIDatePicker.Mode = .date
    
    var actionHandler: ActionHandler?
    
    var toolbarTitle: String? {
        get { (inputAccessoryView as? Toolbar)?.title }
        set { (inputAccessoryView as? Toolbar)?.title = newValue }
    }
    var date: Date? {
        get { (text ?? "") == "" ? nil : datePicker.date }
        set {
            guard let date = newValue else {
                text = ""
                return
            }
            datePicker.date = date
            dateChanged()
        }
    }
    
    lazy var datePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = datePickerMode
        picker.addTarget(self, action: #selector(dateChanged), for: .editingDidBegin)
        picker.addTarget(self, action: #selector(dateChanged), for: .valueChanged)
        picker.locale = locale
        return picker
    }()
    
    lazy var formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = dateFormat
        return formatter
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    func commonInit() {
        customize()
        prepareDatePicker()
    }
    
    private func customize() {
        tintColor = .clear // Скрытие курсора
    }
    
    private func prepareDatePicker() {
        datePicker.tintColor = Color.green
        if #available(iOS 14, *) {
            datePicker.preferredDatePickerStyle = .compact
            let gesture = datePicker.subviews.first?.gestureRecognizers?.last
            addGestureRecognizer(gesture!)
            datePicker.alpha = 0
            addSubview(datePicker)
            delegate = self
        } else {
            inputView = datePicker
            addToolbar()
            datePicker.backgroundColor = .white
        }
    }
    
    @objc private func touchUpDone() {
        endEditing(true)
        dateChanged()
    }
    
    @objc private func dateChanged() {
        editingChanged()
        text = formatter.string(from: datePicker.date)
        actionHandler?(self, datePicker.date)
    }
    
}

extension DateField: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if #available(iOS 13.4, *) {
            return false
        } else {
            return true
        }
    }
}
