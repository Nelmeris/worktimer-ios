//
//  TestFields.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import UIKit

class TextField: UITextField {
    override init(frame: CGRect) {
        super.init(frame: frame)
        prepare()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        prepare()
    }
    
    func prepare() {
        cornerRadius = 6
        borderWidth = 1.2
        prepareText()
        addTarget(self, action: #selector(editingChanged), for: .editingDidBegin)
        addTarget(self, action: #selector(editingChanged), for: .editingDidEnd)
        editingChanged()
    }
    
    private func prepareText() {
        textColor = R.color.blackA()
        guard let font = ButtonLabel().font else { return }
        self.font = font
        attributedPlaceholder = .init(string: placeholder ?? "",
                                      attributes: [
                                        .foregroundColor: (Color.gray) as Any,
                                        .font: font
                                      ])
    }
    
    @objc func editingChanged() {
        if isEditing {
            borderColor = Color.green
        } else {
            borderColor = Color.gray
        }
    }
}

final class PasswordField: TextField {
    override func prepare() {
        addEye()
        super.prepare()
    }
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        CGRect(x: bounds.width - bounds.height, y: 0,
               width: bounds.height, height: bounds.height)
    }
    
    private func addEye() {
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: frame.height, height: frame.height)
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(touchUpEye))
        button.addGestureRecognizer(gestureRecognizer)
        button.imageView?.contentMode = .center
        rightView = button
        rightViewMode = .always
        updateEyeImage()
    }
    
    @objc private func touchUpEye() {
        isSecureTextEntry.toggle()
        updateEyeImage()
    }
    
    private func updateEyeImage() {
        guard let button = rightView as? UIButton else { return }
        button.setImage(isSecureTextEntry ? R.image.eyeClose() : R.image.eyeOpen(),
                        for: .normal)
    }
    
    override func editingChanged() {
        super.editingChanged()
        guard let button = rightView as? UIButton else { return }
        button.tintColor = isEditing ? Color.green : Color.gray
    }
}
