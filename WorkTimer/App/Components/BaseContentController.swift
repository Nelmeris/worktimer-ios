//
//  BaseContentController.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import UIKit

protocol ContentControllerProtocol {
    var rootContainer: UIStackView! { get }
    var refreshHandler: () -> Void { get }
}

extension ContentControllerProtocol {
    func showNetworkError() {
    }
    
    func showNotFound() {
    }
    
    func hideSplash() {
    }
}

extension BaseContentController {
    enum FilterButtonState {
        case active, `default`, hidden
    }
    
    enum State {
        case load, search, networkError, notFound, `default`, fetching
    }
}

class BaseContentController<Cell: PreparebleCell>: UIViewController, UITableViewDelegate, UITableViewDataSourcePrefetching, UISearchBarDelegate, ContentControllerProtocol {
    @IBOutlet private(set) weak var rootContainer: UIStackView!
    @IBOutlet private(set) weak var searchBar: UISearchBar!
    @IBOutlet private(set) weak var bottomConstraint: NSLayoutConstraint!
    
    private(set) weak var tableView: TableView<Cell>!
    private(set) weak var bottomActivityView: UIActivityIndicatorView!
    
    private(set) var state: State?
    var viewModels: [Cell.ViewModel] = []
    
    var filterManager: BaseFilterManager? { fatalOverride() }
    var filterKeywords: String? {
        get { fatalOverride() }
        set { fatalOverride() }
    }
    private var searchTimer: Timer?
    var canRefresh: Bool { true }
    var canFetch: Bool { true }
    var canFilter: Bool { true }
    var canSearch: Bool { true }
    var searchPlaceholder: String { "Поиск" }
    var customNavigationItem: UINavigationItem { navigationItem }
    var refreshHandler: () -> Void { handleNetworkRefresh }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        filterManager?.loadFitlerData(completion: handleLoadFilterDataResult)
        prepareUI()
        subscribeKeyboardEvents(actionHandler: handleKeyboardAction)
        
        setState(.load)
        loadData()
    }
    
    func prepareUI() {
        prepareTable()
        if canRefresh {
            prepareTableRefreshControl()
        }
        if canFetch {
            prepareBottomActivityView()
        }
        if canSearch {
            prepareSearch()
        }
        if canFilter {
            customNavigationItem.addRightButtonsContainer()
        }
    }
    
    // MARK: - Data Processing
    
    func loadData() {
        fatalOverride()
    }
    
    func fetchNextData() {
        guard state == .default else { return }
        setState(.fetching)
    }
    
    func refresh() {
        setState(.load)
        loadData()
        filterManager?.loadFitlerData(completion: handleLoadFilterDataResult)
    }
    
    @objc private func objcRefresh() {
        refresh()
        tableView.refreshControl?.endRefreshing()
    }
    
    func setBottomIndicatorVisible(_ visible: Bool) {
        guard canFetch else { return }
        bottomActivityView.isHidden = !visible
        if visible {
            bottomActivityView.startAnimating()
        } else {
            bottomActivityView.stopAnimating()
        }
    }
    
    // MARK: - Prefetch
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        guard canFetch, let row = indexPaths.first?.row else { return }
        let needsFetch = row >= viewModels.count - 7
        
        if needsFetch, !viewModels.isEmpty, state != .fetching {
            fetchNextData()
        }
    }
    
    // MARK: - Did Select
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        fatalOverride()
    }
    
    // MARK: - Filter
    
    func handleFilterControllerAction(filters: [FilterItemProtocol]) {
        filterManager?.processFilterItems(filters: filters)
        setState(.search)
        loadData()
    }
    
    func handleLoadFilterDataResult(errors: [APICoreError]) {
        errors.forEach {
            print("Ошибка загрузки фильтра", $0.code ?? "", $0.error ?? "")
        }
        refreshFilterButtonState()
    }
    
    @objc func showFilter() {
        guard let filterManager = filterManager else { return }
        view.endEditing(true)
        guard let vc = R.storyboard.filter.filterController() else { fatalError() }
        vc.prepare(with: filterManager.items, root: self)
        presentAsSheet(vc, isDismissable: true)
        vc.actionHandler = handleFilterControllerAction
    }
    
    // MARK: - Search
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchTimer?.invalidate()
        filterKeywords = searchText
        
        searchTimer = Timer.scheduledTimer(withTimeInterval: 0.2, repeats: false) { (timer) in
            timer.invalidate()
            self.setState(.search)
            self.loadData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }

    // MARK: - UI Logic

    func setState(_ state: State) {
        guard self.state != state else { return }
        self.state = state
        
        switch state {
        case .load, .search:
            tableView.setShimmerVisible(true)
            tableView.alpha = 1
        case .default, .fetching, .notFound:
            tableView.setShimmerVisible(false)
            tableView.alpha = 1
        case .networkError:
            tableView.alpha = 0
        }
        
        switch state {
        case .networkError:
            showNetworkError()
        case .notFound:
            showNotFound()
        default:
            hideSplash()
        }
        
        switch state {
        case .load, .networkError:
            searchBar.isHidden = true
            setFilterButtonState(state: .hidden)
        default:
            searchBar.isHidden = !canSearch
            refreshFilterButtonState()
        }
        
        switch state {
        case .fetching:
            setBottomIndicatorVisible(true)
        default:
            setBottomIndicatorVisible(false)
        }
    }
    
    func refreshFilterButtonState() {
        guard let filterManager = filterManager else { return }
        if filterManager.dataLoaded {
            switch state {
            case .networkError, .load:
                setFilterButtonState(state: .hidden)
            default:
                setFilterButtonState(state: filterManager.isDefault ? .default : .active)
            }
        } else {
            setFilterButtonState(state: .hidden)
        }
    }
    
    private func handleNetworkRefresh() {
        refresh()
    }
    
    func handleKeyboardAction(_ action: KeyboardAction, keyboardSize: CGRect) {
        guard action != .changeFrame,
              let globalView = UIApplication.shared.keyWindow?.rootViewController?.view,
              let globalPoint = rootContainer.superview?.convert(rootContainer.frame.origin, to: nil)
        else { return }
        let bottomMarginToRoot = globalView.frame.height - rootContainer.frame.height - globalPoint.y
        bottomConstraint.constant = action == .show ? keyboardSize.height - bottomMarginToRoot : .zero
        view.layoutIfNeeded()
    }
    
    // MARK: - UI
    
    private func prepareTable() {
        let tableView = TableView<Cell>()
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.prefetchDataSource = self
        tableView.keyboardDismissMode = .onDrag
        tableView.delegate = self
        rootContainer.addArrangedSubview(tableView)
        self.tableView = tableView
    }
    
    private func prepareTableRefreshControl() {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(objcRefresh), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    private func prepareBottomActivityView() {
        let bottomActivityView = UIActivityIndicatorView(frame: .init(x: 0, y: 0,
                                                                      width: tableView.frame.width,
                                                                      height: 44))
        self.bottomActivityView = bottomActivityView
        tableView.tableFooterView = bottomActivityView
        setBottomIndicatorVisible(false)
    }
    
    private func prepareSearch() {
        searchBar.placeholder = searchPlaceholder
    }
    
    private func setFilterButtonState(state: FilterButtonState) {
        let buttonTag = 1342
        customNavigationItem.removeButtonFromRightButtons(tag: buttonTag)
        let image: UIImage?
        switch state {
        case .default:
            image = R.image.filterIcon()
        case .active:
            image = R.image.filterActiveIcon()
        case .hidden:
            return
        }
        if !(customNavigationItem.rightBarButtonItem?.customView is UIStackView),
           canFilter{
            customNavigationItem.addRightButtonsContainer()
        }
        customNavigationItem.addButtonToRightButtons(image: image,
                                                     tag: buttonTag,
                                                     toStart: true,
                                                     target: self,
                                                     selector: #selector(showFilter))
    }
    
}
