//
//  Buttons.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import UIKit

class GreenButton: BaseButton {
    override var backgroundColorForState: [StyleButton.State : UIColor?] {
        [
            .default: Color.green,
            .highlighted: Color.greenPressed,
            .disabled: Color.greenDisabled
        ]
    }
    override var titleColorForState: [StyleButton.State : UIColor?] {
        [
            .default: Color.white
        ]
    }
}

class NoneGreenButton: BaseButton {
    override var backgroundColorForState: [StyleButton.State : UIColor?] {
        [
            .default: .clear
        ]
    }
    override var titleColorForState: [StyleButton.State : UIColor?] {
        [
            .default: Color.green,
            .highlighted: Color.greenPressed,
            .disabled: Color.greenDisabled
        ]
    }
}

class WhiteButton: BaseButton {
    override var backgroundColorForState: [StyleButton.State : UIColor?] {
        [
            .default: Color.whiteA,
            .highlighted: Color.graySeparator
        ]
    }
    override var titleColorForState: [StyleButton.State : UIColor?] {
        [
            .default: Color.blackA,
            .disabled: Color.gray
        ]
    }
    override var shadowOpacityForState: [StyleButton.State : Float] {
        [.default: 0.08]
    }
    override var shadowRadiusForState: [StyleButton.State : CGFloat] {
        [.default: 22]
    }
    override var shadowOffsetForState: [StyleButton.State : CGSize] {
        [.default: .init(width: 0, height: 7)]
    }
    override var shadowColorForState: [StyleButton.State : UIColor] {
        [.default: Color.blackA]
    }
}

class BaseButton: StyleButton {
    override var customCornerRadius: CGFloat { 12 }
    override var styleFont: UIFont? { ButtonLabel().font }
}
