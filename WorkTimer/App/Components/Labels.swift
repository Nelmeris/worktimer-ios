//
//  Labels.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import UIKit

class HeroLabel: StyleLabel {
    public override var fontSize: CGFloat { 24.0 }
    public override var fontWeight: UIFont.Weight { .semibold }
}

class TitleLabel: StyleLabel {
    override var fontSize: CGFloat { 18.0 }
    override var fontWeight: UIFont.Weight { .semibold }
}

class ButtonLabel: StyleLabel {
    override var fontSize: CGFloat { 16.0 }
    override var fontWeight: UIFont.Weight { .semibold }
}

class TextLabel: StyleLabel {
    override var fontSize: CGFloat { 15.0 }
    override var fontWeight: UIFont.Weight { .medium }
}

class ParagraphLabel: StyleLabel {
    override var fontSize: CGFloat { 14.0 }
    override var fontWeight: UIFont.Weight { .regular }
    override var lineHeight: CGFloat { 20.0 }
}
