//
//  FilterListItemView.swift
//  Treolan
//
//  Created by Артем Куфаев on 3/8/21.
//  Copyright © 2021 Treolan. All rights reserved.
//

import UIKit

extension FilterListItemView {
    typealias ValueChangeHandler = (Bool, FilterListItemView) -> Void
    
    enum Types {
        case radio, check
    }
    
    struct ViewModel {
        let title: String
        let checked: Bool
        let type: Types
    }
}

final class FilterListItemView: NibView {
    
    @IBOutlet private weak var checkContainer: DesignableView!
    @IBOutlet private weak var checkIconView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    
    var valueChangeHandler: ValueChangeHandler?
    var checked: Bool = false
    var model: ViewModel!
    
    init(with model: ViewModel) {
        super.init(frame: .zero)
        commonInit()
        prepare(with: model)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(touchUp))
        addGestureRecognizer(gestureRecognizer)
    }
    
    func prepare(with model: ViewModel) {
        self.model = model
        titleLabel.text = model.title
        checkContainer.isRounded = model.type == .radio
        setChecked(model.checked)
    }
    
    @objc private func touchUp() {
        setChecked(!checked)
        valueChangeHandler?(checked, self)
    }
    
    func setChecked(_ checked: Bool) {
        self.checked = checked
        checkIconView.isHidden = !checked
    }
    
}
