//
//  FilterListController.swift
//  Treolan
//
//  Created by Артем Куфаев on 3/8/21.
//  Copyright © 2021 Treolan. All rights reserved.
//

import UIKit

extension FilterListController {
    typealias ActionHandler = ([String]) -> Void
    
    struct ViewModel {
        let title: String
        let items: [Item]
        let type: FilterListItemView.Types
        
        struct Item {
            let title: String
            let checked: Bool
        }
    }
}

extension FilterListItemView.ViewModel {
    init(model: FilterListController.ViewModel.Item, type: FilterListItemView.Types) {
        self.init(title: model.title, checked: model.checked, type: type)
    }
}

final class FilterListController: UIViewController {
    
    @IBOutlet private weak var rootContainer: UIStackView!
    @IBOutlet private weak var itemsContainer: UIStackView!
    
    private var model: ViewModel!
    private var bottomMargin: CGFloat!
    var items: [FilterListItemView] {
        itemsContainer.arrangedSubviews.compactMap { $0 as? FilterListItemView }
    }
    var actionHandler: ActionHandler!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bottomMargin = rootContainer.layoutMargins.bottom
        subscribeKeyboardEvents(actionHandler: handleKeyboardAction)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        actionHandler?(items.compactMap { $0.checked ? $0.model.title : nil })
    }
    
    func prepare(with model: ViewModel) {
        loadViewIfNeeded()
        self.model = model
        title = model.title
        setItems(model.items)
    }
    
    private func setItems(_ items: [ViewModel.Item]) {
        items.enumerated().forEach { index, itemModel in
            let view = FilterListItemView(with: .init(model: itemModel, type: model.type))
            view.backgroundColor = index % 2 != 0 ? Color.graySeparator : .clear
            itemsContainer.addArrangedSubview(view)
            view.valueChangeHandler = handleValueChange
        }
    }
    
    private func handleKeyboardAction(_ action: KeyboardAction, keyboardSize: CGRect) {
        rootContainer.layoutMargins.bottom = bottomMargin
        if action == .show {
            rootContainer.layoutMargins.bottom += keyboardSize.height - view.safeAreaInsets.bottom
        }
        view.layoutIfNeeded()
    }
    
    private func handleValueChange(_ checked: Bool, item: FilterListItemView) {
        guard model.type == .radio else { return }
        items.forEach {
            guard item != $0 else { return }
            $0.setChecked(false)
        }
    }
    
}

// MARK: - Search

extension FilterListController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        itemsContainer.removeAllArrangedSubviews()
        if searchText.isEmpty {
            setItems(model.items)
        } else {
            setItems(model.items.filter({
                $0.title.lowercased().contains(searchText.lowercased())
            }))
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }
}
