//
//  FilterItemView.swift
//  Treolan
//
//  Created by Артем Куфаев on 3/8/21.
//  Copyright © 2021 Treolan. All rights reserved.
//

import UIKit

extension FilterItemView {
    typealias TapHandler = (FilterItemView) -> Void
    
    typealias ViewModel = FilterListController.ViewModel
}

final class FilterItemView: NibView, FilterItemProtocol {
    
    @IBOutlet private weak var leftContainer: UIStackView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var selectedLabel: UILabel!
    @IBOutlet private weak var resetButton: UIButton!
    
    var changeHandler: (() -> Void)?
    var isDefault: Bool { resetButton.isHidden }
    
    var model: ViewModel?
    var tapHandler: TapHandler!
    var selectedItem: Any?
    var selectedItems: [String] = []
    
    init(model: FilterListController.ViewModel) {
        super.init(frame: .zero)
        commonInit()
        prepare(with: model)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalOverride()
    }
    
    private func commonInit() {
        let gestureRecognizer = UITapGestureRecognizer(target: self,
                                                       action: #selector(touchUpFilter))
        leftContainer.addGestureRecognizer(gestureRecognizer)
        reset()
    }
    
    func prepare(with model: ViewModel) {
        self.model = model
        titleLabel.text = model.title
        setSelectedValues(model.items.compactMap { $0.checked ? $0.title : nil })
    }
    
    func setSelectedValues(_ values: [String]) {
        self.selectedItems = values
        resetButton.isHidden = values.isEmpty
        if values.isEmpty {
            selectedLabel.text = "Не выбрано"
            selectedLabel.textColor = Color.gray
        } else if values.count == 1, let value = values.first {
            selectedLabel.text = value
            selectedLabel.textColor = Color.green
        } else {
            selectedLabel.text = "\(values.count) элемента выбрано"
            selectedLabel.textColor = Color.green
        }
    }
    
    func reset() {
        setSelectedValues([])
        changeHandler?()
    }
    
    @IBAction private func touchUpReset(_ sender: UIButton) {
        reset()
    }
    
    @objc private func touchUpFilter() {
        tapHandler(self)
    }
    
}
