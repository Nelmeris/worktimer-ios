//
//  FilterItemProtocol.swift
//  Treolan
//
//  Created by Артем Куфаев on 3/8/21.
//  Copyright © 2021 Treolan. All rights reserved.
//

import UIKit

protocol FilterItemProtocol: UIView {
    var isDefault: Bool { get }
    var changeHandler: (() -> Void)? { get set }
    func reset()
}
