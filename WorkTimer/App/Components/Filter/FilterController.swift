//
//  FilterController.swift
//  Treolan
//
//  Created by Артем Куфаев on 3/8/21.
//  Copyright © 2021 Treolan. All rights reserved.
//

import UIKit

extension FilterController {
    typealias ActionHandler = ([FilterItemProtocol]) -> Void
}

final class FilterController: UIViewController {
    
    @IBOutlet private weak var filterItemsContainer: UIStackView!
    @IBOutlet private weak var resetAllButton: UIButton!
    
    private var filterItems: [FilterItemProtocol]!
    private weak var root: UIViewController?
    
    var actionHandler: ActionHandler!
    
    var resetEnabled: Bool {
        get { resetAllButton.isEnabled }
        set { resetAllButton.isEnabled = newValue }
    }
    
    func prepare(with filterItems: [FilterItemProtocol], root: UIViewController) {
        loadViewIfNeeded()
        self.filterItems = filterItems
        self.root = root
        filterItems.forEach {
            filterItemsContainer.addArrangedSubview($0)
            $0.changeHandler = {
                self.refreshReset()
            }
            if let item = $0 as? FilterItemView {
                item.tapHandler = handleFilterDefaultTap
            }
        }
        refreshReset()
    }
    
    @IBAction func touchUpResetAll(_ sender: UIButton) {
        resetEnabled = false
        filterItems.forEach { $0.reset() }
    }
    
    @IBAction func touchUpApply(_ sender: UIButton) {
        actionHandler(filterItems)
        dismiss(animated: true)
    }
    
    private func handleFilterDefaultTap(item: FilterItemView) {
        guard let vc = R.storyboard.filter.filterListController(),
              let model = item.model else { fatalError() }
        dismiss(animated: true)
        vc.prepare(with: .init(title: model.title,
                               items: model.items.map {
                                .init(title: $0.title, checked: item.selectedItems.contains($0.title))
                               },
                               type: model.type))
        vc.actionHandler = { items in
            item.setSelectedValues(items)
            self.refreshReset()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.root?.presentAsSheet(self, isDismissable: true)
            }
        }
        self.root?.push(vc)
    }
    
    private func refreshReset() {
        resetEnabled = filterItems.contains { !$0.isDefault }
    }
    
}
