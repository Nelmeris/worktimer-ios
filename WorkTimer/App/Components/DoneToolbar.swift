//
//  DoneToolbar.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import UIKit

public extension Toolbar {
    struct Options {
        let target: Any
        let action: Selector
        
        public init(target: Any,
                    action: Selector) {
            
            self.target = target
            self.action = action
        }
    }
}

public class Toolbar: UIToolbar {
    public var title: String? {
        get { titleLabel.text }
        set { titleLabel.text = newValue }
    }
    
    private weak var titleLabel: UILabel!
    
    public init(with options: Options) {
        super.init(frame: CGRect(x: 0, y: 0,
                                 width: UIScreen.main.bounds.width,
                                 height: 50.0))
        commonInit(with: options)
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit(with: .init(target: self, action: #selector(emptyAction)))
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit(with: .init(target: self, action: #selector(emptyAction)))
    }
    
    private func commonInit(with options: Options) {
        barStyle = .default
        backgroundColor = .white
        barTintColor = .white
        
        let label = TitleLabel()
        let titleItem = UIBarButtonItem(customView: label)
        titleLabel = label
        
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let button = UIBarButtonItem(title: "Готово",
                                     style: .done,
                                     target: options.target,
                                     action: options.action)
        button.setTitleTextAttributes([
            NSAttributedString.Key.font: ButtonLabel().font!,
            NSAttributedString.Key.foregroundColor: Color.green
        ], for: .normal)
        
        items = [titleItem, spacer, button]
        sizeToFit()
    }
    
    @objc private func emptyAction() {}
}

extension UITextField {
    public func addToolbar() {
        inputAccessoryView = Toolbar(with: .init(target: self,
                                                 action: #selector(closeKeyboard)))
    }
    
    @objc private func closeKeyboard() {
        endEditing(true)
    }
}
