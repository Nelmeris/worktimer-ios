//
//  NavRightButtonsContainer.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 10.07.2021.
//

import UIKit

extension UINavigationItem {
    func addRightButtonsContainer() {
        let stackview = UIStackView()
        stackview.axis = .horizontal
        stackview.layoutMargins.right = 0 // Ручной подбор для симметрии
        stackview.isLayoutMarginsRelativeArrangement = true
        stackview.spacing = 4
        rightBarButtonItem = UIBarButtonItem(customView: stackview)
        stackview.heightAnchor.constraint(equalToConstant: 28).isActive = true // 24 + 8 / 2
    }
    
    func addButtonToRightButtons(image: UIImage?, tag: Int = 0, toStart: Bool = false, target: Any, selector: Selector) {
        let button = UIButton(type: .custom)
        button.tag = tag
        button.setImage(image, for: .normal)
        button.addTarget(target, action: selector, for: .touchUpInside)
        let stack = rightBarButtonItem?.customView as? UIStackView
        if toStart {
            stack?.insertArrangedSubview(button, at: 0)
        } else {
            stack?.addArrangedSubview(button)
        }
        button.aspectRation(1).isActive = true
    }
    
    func removeButtonFromRightButtons(tag: Int) {
        let stack = (rightBarButtonItem?.customView as? UIStackView)
        guard let view = stack?.arrangedSubviews.first(where: { $0.tag == tag }) else { return }
        stack?.removeArrangedView(view)
    }
}
