//
//  AppDelegate.swift
//  WorkTimer
//
//  Created by Артем Куфаев on 09.07.2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let window = UIWindow(frame: UIScreen.main.bounds)
        self.window = window
        AppCore.shared.prepare(window: window)
        AppCore.shared.run()
        return true
    }
    
}

